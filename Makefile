########## Apps ##############

#### Infininode ###########################################################################

install-node:
	docker-compose run --rm infininet-node "npm install && cp -f .env.devcontainer .env"

code-node:
	devcontainer open apps/node

start-node:
	docker-compose up -d --force-recreate infininet-node

logs-node:
	docker-compose logs -f infininet-node



#### Infininode Client development
code-server-sdk:
	devcontainer open apps/server-sdk

code-web-sdk:
	devcontainer open apps/web-sdk

code-admin:
	devcontainer open apps/admin

code-webpage:
	devcontainer open apps/webpage

#### Service ###########
install-base:
	# create local network
	docker network create infininet | :


install: install-base install-node

start:
	docker-compose up -d --remove-orphans

stop:
	docker-compose stop

remove:
	docker-compose down --remove-orphans

logs:
	docker-compose logs -f

ps:
	docker-compose ps
