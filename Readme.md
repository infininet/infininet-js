# Production

## Infininet Node
InfiniNet Node is available as Docker Image at
```
registry.gitlab.com/infininet/infininet-js:node-master
```

## Server SDK
The SDK for NodeJS based apps is available as NPM Module
```
npm install infininet-server-sdk
```

## Web SDK
The SDK for web based apps is available as NPM Module
```
npm install infininet-web-sdk
```



# Development Environment

## Requirements
* Docker and Docker Compose
* Devcontainer as executable
* Softlink devcontainer binary path to /usr/bin/devcontainer

## Install
```
make install
```

## Run all containers
```
make start
```

## Start/Restart one container
```
make start-{app name}
```

## Stop all containers
```
make stop
```

## Remove all containers
```
make remove
```

## Run VS code environment
```
make code-{app name}
```

## Log for all containers
```
make logs
```

## Log for one container
```
make logs-{app name}
```
