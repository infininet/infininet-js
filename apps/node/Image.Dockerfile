FROM ubuntu:18.04
USER root

RUN apt update
RUN apt install -y nginx npm curl
RUN npm install -g n
RUN n 16.3.0

# This Imagefile is available at registry.gitlab.com/infininet/infininet-js:images_node-base-image
