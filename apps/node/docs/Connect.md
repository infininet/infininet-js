# InfiniNet Connection
> Protocol 0.0.0 :  

## Step 1.  
Send Protocol Version to the Net  
```
CLIENT >> `{d}.{d}.{d}` version number of the client Protocol  
CLIENT << `ACK`  If Version is supported  
-- ELSE --
CLIENT << `ERR.init.0000.<error-message>` and disconnect  
```

## Step 2. 
Send App Identifier of the app you want to connect  
```
CLIENT >> `alphanumerappidentifier`  
CLIENT << `ACK` If Identifier is valid  
-- ELSE --
CLIENT << `ERR.init.0000.<error-message>` and disconnect     
```

## Step 3. 
Send Network Identifier you want to be connected with
```
CLIENT >> `alphanumericnetworkidentifier`  
CLIENT << `ACK` If Network is valid  
-- ELSE --
CLIENT << `ERR.init.0000.<error-message>` and disconnect    
```

## Step 4.  
Send your desired Identifier you want to be reachable at
```
CLIENT >> `hostidentifier`  
CLIENT << `HELLO;` If Identifier is valid and unique  
-- ELSE --
CLIENT << `ERR.init.0000.<error-message>` and disconnect  
``` 
