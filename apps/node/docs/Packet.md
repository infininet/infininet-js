# Packet

The Packet is a structured String concatenated with `.` and terminated with `;`

### Parts:
1. __Kind__  
    A predefined key indicating the Message kind. Allowed are:  
    * `MSG` Message for a Client
    * `INF` Info Request to get a Recipient Information
    * `RES` Response to a previous send Request
    * `ERR` Error notification from InfiniNet. Cannot be used by a Client
2. __Request ID__  
    Custom Request ID defined by the Client
3. __Sender__  
    Public Address of the sending Client
4. __Recipient__  
    Public Address of the destination Client
5. __Payload__  
    Payload to be transmitted

### Example  
`KIND`.`hashFromHeaderAndPayload`.`encodedHeader`.`encodedPayload`;

### Example for `MSG`:
```js
    let terminator = ';';
    let requestId = createUniqueString();
    let sender = '65s4df6g46s5d7fgs4th657gjf6gh4j9e8trzw5er4gh';
    let recipient = '0s0d9fg80s98gh98dfg09h8d0f9gh80d9fgh80d9f8gh90';
    let payload = "YmFzZTY0RW5jb2RlZFN0dWZm";

    let msgString = [
        'MSG',          // Indicates the kind of the message
        requestId,      // Unique Request Identifier
        sender,         // Sender
        recipient,      // Recipient
        payload,        // Any kind of base64 encoded Data
        terminator,     // Message Terminator
    ].join('.');

```

Packet: `MSG.97ca9a1af77ee6e5715a7554137bda06.65s4df6g46s5d7fgs4th657gjf6gh4j9e8trzw5er4gh.0s0d9fg80s98gh98dfg09h8d0f9gh80d9fgh80d9f8gh90.YmFzZTY0RW5jb2RlZFN0dWZm;`

## Response  

### Successful:
> No Response is send by default. The Response will be send by the receiving Client as `RES.<your-request-id>.<responding-client>.<your-public-address>.<payload>;`

### Failing
> A failed Transmission will send back and Error Package: `ERR.<your-request-id>.<error-code>.<base-64-error-message>;`

1. #### General Errors 
    * Internal Error  
        `0000` > `disconnect`

2. #### Kind MSG 
    * `1000` > Internal Error  
    * `1001` > Unknown Recipient  
    * `1002` > Transmission failed  

