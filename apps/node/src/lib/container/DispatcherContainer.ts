import IDispatcherContainer from '@ext/infininet/lib/peertopeer/IDispatcherContainer';
import IMessageFactory from '@ext/infininet/lib/peertopeer/message/IMessageFactory';
import MessageFactory from '@ext/infininet/lib/peertopeer/message/MessageFactory';

export default class DispatcherContainer implements IDispatcherContainer {
    public getMessageFactory(): IMessageFactory {
        return new MessageFactory();
    }
}
