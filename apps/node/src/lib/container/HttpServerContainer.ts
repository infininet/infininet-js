import ISocket from '@ext/infininet/lib/stream/ISocket';
import IReader from '@ext/infininet/lib/stream/IReader';
import IWriter from '@ext/infininet/lib/stream/IWriter';
import AbstractContainer from '@ext/infininet/lib/communicator/AbstractContainer';
import IHttpContainer from '@ext/infininet/lib/httpserver/IHttpContainer';
import IConnectionFactory from '@ext/infininet/lib/communicator/IConnectionFactory';
import Reader from '@ext/infininet/lib/stream/Reader';
import Writer from '@ext/infininet/lib/stream/Writer';
import ConnectionFactory from '@ext/infininet/lib/communicator/ConnectionFactory';
import { IRequestRouter } from '@src/ext/infininet/lib/httpserver/request/IRequestRouter';
import Router from '@lib/router/Router';
import { registerRoutes } from '@src/routes/registry';
import { IRepository } from '@src/ext/infininet/lib/environment/IRepository';

export default class HttpServerContainer extends AbstractContainer implements IHttpContainer {
    private port: number;
    private httpRouter: IRequestRouter;
    private environmentRepository: IRepository;

    public constructor(
        options: { port: number; managerKey: string; registerRoutes: (router: IRequestRouter) => void },
        environmentRepository: IRepository,
    ) {
        super();
        this.port = options.port;
        this.httpRouter = new Router(options.managerKey);
        this.environmentRepository = environmentRepository;

        registerRoutes(this.httpRouter);
    }

    public getHttpRouter(): IRequestRouter {
        return this.httpRouter;
    }

    public getConnectionFactory(): IConnectionFactory {
        return new ConnectionFactory();
    }

    public getEnvironmentRepository(): IRepository {
        return this.environmentRepository;
    }

    public getPort(): number {
        return this.port;
    }

    public createReader(socket: ISocket): IReader {
        return new Reader({ socket: socket });
    }

    public createWriter(socket: ISocket): IWriter {
        return new Writer({ socket: socket });
    }
}
