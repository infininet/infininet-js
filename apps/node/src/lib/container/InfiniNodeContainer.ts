import IInfiniNodeContainer from '@ext/infininet/lib/IInfiniNodeContainer';
import IDispatcher from '@ext/infininet/lib/peertopeer/IDispatcher';
import Dispatcher from '@ext/infininet/lib/peertopeer/Dispatcher';
import Server from '@ext/infininet/lib/Server';
import IConnector from '@ext/infininet/lib/IConnector';
import IServer from '@ext/infininet/lib/IServer';
import NetServerContainer from '@lib/container/NetServerContainer';
import NetConnector from '@ext/infininet/lib/netserver/Connector';
import DispatcherContainer from '@lib/container/DispatcherContainer';

import HttpConnector from '@ext/infininet/lib/httpserver/Connector';
import HttpServerContainer from '@lib/container/HttpServerContainer';
import { IRequestRouter } from '@src/ext/infininet/lib/httpserver/request/IRequestRouter';
import { IEnvironmentSetup } from '@ext/infininet/lib/IEnvironment';
import { IRepository } from '@src/ext/infininet/lib/environment/IRepository';
import { EnvironmentFactory } from '@src/ext/infininet/lib/environment/EnvironmentFactory';
import { NetworkFactory } from '@src/ext/infininet/lib/peertopeer/NetworkFactory';

export interface IContainerOptions {
    environments: IEnvironmentSetup[];
    netServer: {
        activate: boolean;
        port: number;
    };
    httpServer: {
        managerKey: string;
        activate: boolean;
        port: number;
        registerRoutes: (router: IRequestRouter) => void;
    };
}

export default class InfiniNodeContainer implements IInfiniNodeContainer {
    protected connectorSet: Set<IConnector> = new Set<IConnector>();
    protected options: IContainerOptions;
    protected environmentRepository?: IRepository;

    public constructor(options: IContainerOptions) {
        this.options = options;
    }

    protected getEnvironmentRepository(): IRepository {
        if (!this.environmentRepository) {
            this.environmentRepository = (new EnvironmentFactory()).generateRepository(
                this.options.environments, new NetworkFactory((new DispatcherContainer()).getMessageFactory()),
            );
        }

        return this.environmentRepository;
    }

    public getServer(): IServer {
        return new Server();
    }

    public getDispatcher(): IDispatcher {
        return new Dispatcher(new DispatcherContainer(), this.getEnvironmentRepository());
    }

    public getConnectorSet(): Set<IConnector> {
        return this.connectorSet;
    }

    public initConnectors(): void {
        if (this.options.netServer.activate) {
            this.connectorSet.add(new NetConnector(new NetServerContainer(this.options.netServer, this.getEnvironmentRepository())));
        }
        if (this.options.httpServer.activate) {
            this.connectorSet.add(new HttpConnector(new HttpServerContainer(this.options.httpServer, this.getEnvironmentRepository())));
        }
    }
}
