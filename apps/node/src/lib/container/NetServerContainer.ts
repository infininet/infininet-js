import INetServerContainer from '@ext/infininet/lib/netserver/INetServerContainer';
import ISocket from '@ext/infininet/lib/stream/ISocket';
import IReader from '@ext/infininet/lib/stream/IReader';
import Reader from '@ext/infininet/lib/stream/Reader';
import Writer from '@ext/infininet/lib/stream/Writer';
import IWriter from '@ext/infininet/lib/stream/IWriter';
import AbstractContainer from '@ext/infininet/lib/communicator/AbstractContainer';
import { IRepository } from '@src/ext/infininet/lib/environment/IRepository';

export default class NetServerContainer extends AbstractContainer implements INetServerContainer {
    private port: number;
    private environmentRepository: IRepository;

    public constructor(options: { port: number }, environmentRepository: IRepository) {
        super();
        this.port = options.port;
        this.environmentRepository = environmentRepository;
    }

    public getPort(): number {
        return this.port;
    }

    public createReader(socket: ISocket): IReader {
        return new Reader({ socket: socket });
    }

    public createWriter(socket: ISocket): IWriter {
        return new Writer({ socket: socket });
    }

    public getEnvironmentRepository(): IRepository {
        return this.environmentRepository;
    }
}
