import { IRequestData } from '@src/ext/infininet/lib/httpserver/request/IRequestData';
import { UnauthorizedError, UnknownRouteError } from '@src/ext/infininet/lib/httpserver/request/IRequestRouter';
import { IRoute } from '@src/ext/infininet/lib/httpserver/request/IRoute';

export default class Router {
    private managerKey: string;
    private routes: IRoute[] = [];

    public constructor(managerKey: string) {
        this.managerKey = managerKey;
    }

    public defineRoute(route: IRoute): void {
        this.routes.push(route);
    }

    protected getRoute(routePath: string): IRoute | null {
        let machedRoutes = this.routes.filter((route: IRoute) => {
            return route.match(routePath);
        });

        return machedRoutes.length > 0 ? machedRoutes[0] : null;
    }

    public checkAuthorization(requestData: IRequestData): void {
        if (this.managerKey !== requestData.headers.managerkey) {
            throw new UnauthorizedError('Unauthorized');
        }
    }

    public async dispatch(requestData: IRequestData): Promise<Record<string, unknown>> {
        this.checkAuthorization(requestData);
        let route = this.getRoute(requestData.route);
        if (!route) {
            throw new UnknownRouteError('Uknown route');
        }

        return route.dispatch({
            params: requestData.params,
            payload: requestData.payload,
        });
    }
}
