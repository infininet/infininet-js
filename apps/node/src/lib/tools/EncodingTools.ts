import crypto from 'crypto';

export function HashMd5(source: string): string {
    return crypto.createHash('md5')
        .update(source)
        .digest('hex');
}

export function Utf8ToBase64(source: string): string {
    return Buffer.from(source, 'utf8').toString('base64');
}

export function Base64ToUtf8(base64: string): string {
    return Buffer.from(base64, 'base64').toString('utf8');
}

export function Serialize<T>(obj: T): string {
    return Utf8ToBase64(JSON.stringify(obj));
}

export function Unserialize<T>(serialized: string): T {
    return JSON.parse(Base64ToUtf8(serialized)) as T;
}

export const BufferFrom = Buffer.from;
