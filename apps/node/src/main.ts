/* eslint-disable @typescript-eslint/no-floating-promises */
require('module-alias/register');

import InfiniNode from '@ext/infininet/InfiniNode';
import InfiniNodeContainer from '@lib/container/InfiniNodeContainer';
import { registerRoutes } from '@src/routes/registry';

import dotenv from 'dotenv';
dotenv.config();

(async (): Promise<void> => {
    let container = new InfiniNodeContainer({
        environments: [
            {
                address: <string>process.env.DEFAULT_ENVIRONMENT,
                networks: [
                    <string>process.env.DEFAULT_NETWORK,
                ],
            },
        ],
        netServer: {
            activate: false,
            port: Number(process.env.INFININODE_UNIX_PORT),
        },
        httpServer: {
            managerKey: <string>process.env.MANAGER_KEY,
            activate: true,
            port: Number(process.env.INFININODE_HTTP_PORT),
            registerRoutes: registerRoutes,
        },
    });
    container.initConnectors();

    let infiniNode = new InfiniNode(container);
    infiniNode.run();
})();
