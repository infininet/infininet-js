import { IRoute } from '@src/ext/infininet/lib/httpserver/request/IRoute';

export class Get implements IRoute {
    public match(routeKey: string): boolean {
        return routeKey === '/ping/';
    }

    public async dispatch(args: { params: Record<string, unknown>; payload?: string }): Promise<Record<string, unknown>> {
        return { received: args };
    }
}
