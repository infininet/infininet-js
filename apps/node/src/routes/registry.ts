import { IRequestRouter } from '@src/ext/infininet/lib/httpserver/request/IRequestRouter';
import ping from './ping';

export function registerRoutes(router: IRequestRouter): void {
    for (let RouteClass of ping) {
        router.defineRoute(new RouteClass());
    }
}
