import IConnector from '@ext/infininet/lib/IConnector';
import Server from '@ext/infininet/lib/Server';

export default class ServerFixture extends Server {
    public getConnectors(): Array<IConnector> {
        return Array.from(this.connectors);
    }
}
