import IDispatcher from '@ext/infininet/lib/peertopeer/IDispatcher';
import IServer from '@ext/infininet/lib/IServer';

export default class DispatcherMock implements IDispatcher {
    public servers: Array<IServer> = [];

    public connectServer(server: IServer): void {
        this.servers.push(server);
    }
}
