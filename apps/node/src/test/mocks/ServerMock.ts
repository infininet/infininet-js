import IConnection from '@ext/infininet/lib/IConnection';
import IConnector from '@ext/infininet/lib/IConnector';
import IServer from '@ext/infininet/lib/IServer';

export default class ServerMock implements IServer {
    public listeners: Array<(connection: IConnection) => void> = [];
    public connectors: Array<IConnector> = [];

    public onConnection(callback: (connection: IConnection) => void): void {
        this.listeners.push(callback);
    }

    public addConnector(connector: IConnector): void {
        this.connectors.push(connector);
    }
}
