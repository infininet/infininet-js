import IInfiniNodeContainer from '@ext/infininet/lib/IInfiniNodeContainer';
import IDispatcher from '@ext/infininet/lib/peertopeer/IDispatcher';
import IConnector from '@ext/infininet/lib/IConnector';
import IServer from '@ext/infininet/lib/IServer';
import DispatcherMock from '@src/test/mocks/DispatcherMock';
import ConnectorMock from '@src/test/mocks/server/ConnectorMock';
import ServerMock from '@src/test/mocks/ServerMock';

export default class InfiniNodeContainerMock implements IInfiniNodeContainer {
    public server: ServerMock;
    public Dispatcher: DispatcherMock;
    public connectorSet: Set<ConnectorMock> = new Set<ConnectorMock>();

    public getServer(): IServer {
        return this.server;
    }

    public getDispatcher(): IDispatcher {
        return this.Dispatcher;
    }

    public getConnectorSet(): Set<IConnector> {
        return this.connectorSet;
    }
}
