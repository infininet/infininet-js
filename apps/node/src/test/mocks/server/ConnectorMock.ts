import IConnection from '@ext/infininet/lib/IConnection';
import IConnector from '@ext/infininet/lib/IConnector';

export default class ConnectorMock implements IConnector {
    public isStartable: boolean = true;

    public isStarted: boolean = false;
    public connectionListener: Array<(connection: IConnection) => void> = [];
    public shutdownListener: Array<(err?: Error) => void> = [];

    public async start(): Promise<boolean> {
        if (this.isStartable) {
            this.isStarted = true;

            return true;
        }

        this.isStarted = false;

        return false;
    }

    public onConnection(callback: (connection: IConnection) => void): void {
        this.connectionListener.push(callback);
    }

    public onShutdown(callback: (err?: Error) => void): void {
        this.shutdownListener.push(callback);
    }

    public simulateShutdown(): void {
        for (let listener of this.shutdownListener) {
            listener();
        }
    }

    public simulateConnection(connection: IConnection): void {
        for (let listener of this.connectionListener) {
            listener(connection);
        }
    }
}
