import ISocket from '@ext/infininet/lib/stream/ISocket';
import EventEmitter from 'node:events';

export default class SocketMock extends EventEmitter implements ISocket {
    public close(): Promise<void> {
        throw new Error('Method not implemented.');
    }

    public onData(callback: (data: string) => void): void {
        throw new Error('Method not implemented.');
    }

    public onceData(callback: (data: string) => void): void {
        throw new Error('Method not implemented.');
    }

    public onDisconnect(callback: () => void): void {
        throw new Error('Method not implemented.');
    }

    public closed: boolean = false;
    public lastMessage: string = '';

    public end(): void {
        throw new Error('Method not implemented.');
    }

    public pause(): void {
        // Off
    }

    public destroy(): void {
        // Off
    }

    public async write(message: string, callback?: (err?: Error) => void): Promise<void> {
        this.lastMessage = message;

        if (callback) {
            callback();
        }
    }
}
