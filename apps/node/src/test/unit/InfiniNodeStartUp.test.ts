import InfiniNode from '@ext/infininet/InfiniNode';
import InfiniNodeContainerMock from '@src/test/mocks/container/InfiniNodeContainerMock';
import DispatcherMock from '@src/test/mocks/DispatcherMock';
import ConnectorMock from '@src/test/mocks/server/ConnectorMock';
import ServerMock from '@src/test/mocks/ServerMock';

function CreateTestCandidate(): {
    infiniNode: InfiniNode;
    serviceContainer: InfiniNodeContainerMock;
    } {
    let serviceContainer = new InfiniNodeContainerMock();

    serviceContainer.server = new ServerMock();
    serviceContainer.Dispatcher = new DispatcherMock();
    serviceContainer.connectorSet.add(new ConnectorMock());

    return {
        infiniNode: new InfiniNode(serviceContainer),
        serviceContainer: serviceContainer,
    };
}

describe('InfiniNode Boot', () => {
    let succeeding = CreateTestCandidate();

    it('Boot without Errors', async () => {
        await expect(succeeding.infiniNode.run()).resolves.toBeUndefined();
    });

    let failing = CreateTestCandidate();
    failing.serviceContainer.connectorSet.values().next().value.isStartable = false;

    it('Boot throw Error', async () => {
        await expect(failing.infiniNode.run()).rejects.toThrowError();
    });
});

describe('Connecting Objects on boot', () => {
    let { infiniNode, serviceContainer } = CreateTestCandidate();

    it('InfiniNode was successful booted', async () => {
        await infiniNode.run();
        expect(true).toBeTruthy();
    });

    it('Server is connected to Dispatcher', async () => {
        expect(serviceContainer.Dispatcher.servers[0]).toEqual(serviceContainer.server);
    });

    it('All Connectors are started', async () => {
        for (let connector of Array.from(serviceContainer.connectorSet)) {
            expect(connector.isStarted).toBeTruthy();
        }
    });

    it('All Connectors passed to Server', async () => {
        for (let connector of Array.from(serviceContainer.connectorSet)) {
            expect(serviceContainer.server.connectors).toContain(connector);
        }
    });
});
