import ServerFixture from '@src/test/fixtures/server/ServerFixture';
import ConnectorMock from '@src/test/mocks/server/ConnectorMock';

describe('Server Unit', () => {
    it('Server adds Connector', () => {
        let connector = new ConnectorMock();
        let server = new ServerFixture();

        expect(server.getConnectors().length).toBe(0);
        server.addConnector(connector);

        expect(server.getConnectors().length).toBe(1);
    });

    it('Server adds Connector once', () => {
        let connectorA = new ConnectorMock();
        let connectorB = new ConnectorMock();
        let server = new ServerFixture();

        expect(server.getConnectors().length).toBe(0);

        server.addConnector(connectorA);
        expect(server.getConnectors().length).toBe(1);

        server.addConnector(connectorA);
        expect(server.getConnectors().length).toBe(1);

        server.addConnector(connectorB);
        expect(server.getConnectors().length).toBe(2);

        server.addConnector(connectorB);
        expect(server.getConnectors().length).toBe(2);
    });

    it('Server removes Connectors which are down', async () => {
        let connectorA = new ConnectorMock();
        let server = new ServerFixture();
        expect(server.getConnectors().length).toBe(0);

        server.addConnector(connectorA);
        expect(server.getConnectors().length).toBe(1);

        connectorA.simulateShutdown();
        expect(server.getConnectors().length).toBe(0);
    });

    it('Server removes Connectors which are down', async () => {
        let connectorA = new ConnectorMock();
        let server = new ServerFixture();
        expect(server.getConnectors().length).toBe(0);

        server.addConnector(connectorA);
        expect(server.getConnectors().length).toBe(1);

        connectorA.simulateShutdown();
        expect(server.getConnectors().length).toBe(0);
    });
});
