import { Base64ToUtf8, HashMd5, Serialize, Unserialize, Utf8ToBase64 } from '@lib/tools/EncodingTools';

describe('Encoding Tools - MD5 Hashing', () => {
    it('Strings are encoded to valid md5', () => {
        let strings = {
            'a5bb20d8da96096da12aab2c7e783d5a': 'encode me',
            '79dd9c67e9b12493b02f92974524ff45': 'encode me too',
            '5453f35d7ac37c95df5087e157b1ffbd': '6df4gh96ed7rtz8u7ed6f45gh96df87zu9d54gh65sdf4gh5',
        };

        for (let hash in strings) {
            expect(HashMd5(strings[hash])).toBe(hash);
        }
    });
});

describe('Encoding Tools - Base64', () => {
    it('Strings are encoded and decoded', () => {
        let strings = {
            'ZW5jb2RlIG1l': 'encode me',
            'ZW5jIidvw7Z7fGRlIG1lIHRvbw==': 'enc"\'oö{|de me too',
            'NmRmNGdoOTZlZDdydHogOHU3ZWQ2ZjQgNWdoOTZkZjg3IHp1OWQ1NGdoNjUgc2RmNGdoNQ==': '6df4gh96ed7rtz 8u7ed6f4 5gh96df87 zu9d54gh65 sdf4gh5',
        };

        for (let hash in strings) {
            // Encoding
            expect(Utf8ToBase64(strings[hash])).toBe(hash);

            // Decoding
            expect(Base64ToUtf8(hash)).toBe(strings[hash]);
        }
    });
});

describe('Encoding Tools - Serialization', () => {
    it('Objects can be serialized and unserialized', () => {
        interface ITestInterface {
            var1: string;
            var2: string;
        }

        let objects: Record<string, ITestInterface> = {};

        for (let serialized in objects) {
            // Decoding
            let original = objects[serialized];
            let decoded = Unserialize<ITestInterface>(serialized);

            expect(decoded).toBeInstanceOf(original);
            expect(decoded.var1).toBe(original.var1);
            expect(decoded.var2).toBe(original.var2);

            // Encoding
            let encoded = Serialize<ITestInterface>(original);
            expect(encoded).toBe(serialized);
        }
    });

    it('Failed unserialization throws Error', () => {
        expect(() => {
            Unserialize('not a valid serialized object');
        }).toThrow();
    });
});
