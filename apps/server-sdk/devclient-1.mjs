import serverSdk from './dist/index.js';
const client = serverSdk.default.getClient();

import http from 'http';
import fs from 'fs';

let server = http.createServer(async (req, res) => {
    console.log('Got Request', req.headers);

    req.on('data', (data) => {
        console.log(data.toString());
    });


    (await fs.createReadStream('./text.csv')).pipe(res);
    // res.write()
    // res.writeHead(201);
    // res.end(JSON.stringify({ proxy: true }));
});

server.listen(9874);

(async () => {
    await client.init({
            host: 'infininet-node:7777',
            version: '1.0.0',
            app: 'customerapp-1',
            environment: 'testenvironment',
            network: 'testnetwork',
            secure: false,
            proxy: {
                host: '0.0.0.0',
                port: 9874,
            }
        },
    );

    client.onNetworkOffline(async () => {
        console.log('network offline');
    });
    client.onNetworkOnline(async () => {
        console.log('network online');
    });

    client.on('ping.me', (req) => {
        console.log('got action request ping.me', req.getParams());

        req.respond(null, { user: 'infinuser' });
    });
})();
