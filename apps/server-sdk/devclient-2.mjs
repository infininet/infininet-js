import serverSdk from './dist/index.js';
const client = serverSdk.default.getClient();

(async () => {
    await client.init(
        {
            host: 'infininet-node:7777',
            version: '1.0.0',
            app: 'customerapp-2',
            environment: 'testenvironment',
            network: 'testnetwork',
            secure: false,
        },
    );

    let runRequest = async () => {
        try {
            let res = await client.rest('customerapp-1').get('/lang', { project: 'crm', lang: 'DE' });

            console.log('response', res);
        } catch (err) {
            console.log(err.message);
        }
    }

    client.onNetworkOffline(async () => {
        console.log('network offline');
    });

    client.onNetworkOnline(async () => {
        console.log('network online')
        runRequest();
    });

    if (client.isNetworkOnline()) {
        runRequest();
    }
})();
