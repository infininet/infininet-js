# Connection protocol to another Endpoint

## Outgoing

### ----- PLAIN ----

SELF >> publicKey >> PEER // Send own public Key to the PEER  
SELF << publicKey << PEER // Receive the PEER public Key
```js
    let payload = {
        action: 'peer',
        params: {
            publicKey: '<string>',
        },
    };
```


### ----- ENCRYPTED ----

SELF >> HELLO >> PEER // Send encrypted HELLO to confirm the peering  
SELF << HELLO << PEER // Receive a encrypted HELLO from PEER  