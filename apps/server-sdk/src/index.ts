export * from './ext/infininet/lib/InfiniClient';
export * from './ext/infininet/lib/communicator/incomming/IIncomingRequest';

import { InfiniClient } from './ext/infininet/lib/InfiniClient';
import WebSocketNodeContainer from './lib/container/WebSocketNodeContainer';

let client = new InfiniClient((new WebSocketNodeContainer()).getConnector());

let exp = {
    getClient(): InfiniClient {
        return client;
    },
    InfiniClient,
};

export default exp;
