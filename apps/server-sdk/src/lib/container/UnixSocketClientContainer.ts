import Communicator from './../../ext/infininet/lib/Communicator';
import Connector from './../../ext/infininet/lib/communicator/Connector';
import KeyGenerator from './../crypto/KeyGenerator';
import Dialer from './../netclient/Dialer';
import ConnectionFactory from './../../ext/infininet/lib/dialer/ConnectionFactory';
import IConnector from './../../ext/infininet/lib/IConnector';
import IContainer from './../../ext/infininet/lib/IContainer';

import Encoder from './../tools/Encoder';

export default class UnixSocketClientContainer implements IContainer {
    public getConnector(): IConnector {
        return new Connector(
            new ConnectionFactory(),
            new Dialer(),
            new Communicator(new KeyGenerator(), new Encoder()),
        );
    }
}
