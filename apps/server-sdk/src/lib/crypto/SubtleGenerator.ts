import { ICryptoKey, ICryptoKeyPair } from './../../ext/infininet/lib/crypto/ISubtle';
import crypto from 'crypto';

let subtle = (<any>crypto).webcrypto.subtle;

export default class SubtleGenerator {
    public static async GenerateRsaKeyPair(): Promise<ICryptoKeyPair> {
        return await subtle.generateKey(
            {
                name: 'RSA-OAEP',
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: 'SHA-256',
            },
            true,
            ['encrypt', 'decrypt'],
        );
    }

    public static async GenerateRsaWrapKeyPair(): Promise<ICryptoKeyPair> {
        return await subtle.generateKey(
            {
                name: 'RSA-OAEP',
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: 'SHA-256',
            },
            true,
            ['wrapKey', 'unwrapKey'],
        );
    }

    public static async GenerateAesKey(): Promise<ICryptoKey> {
        return await subtle.generateKey(
            {
                name: 'AES-GCM',
                length: 256,
            },
            true,
            ['encrypt', 'decrypt'],
        );
    }

    public static async WrapAesKey(aesKey: ICryptoKey, publicKey: ICryptoKey): Promise<string> {
        return Buffer.from(await subtle.wrapKey(
            'raw',
            aesKey,
            publicKey,
            {
                name: 'RSA-OAEP',
                hash: { name: 'SHA-256' },
            },
        )).toString('base64');
    }

    public static async UnwrapAesKey(base64AesKey: string, privateKey: ICryptoKey): Promise<ICryptoKey> {
        return subtle.unwrapKey(
            'raw',
            Buffer.from(base64AesKey, 'base64'),
            privateKey,
            {
                name: 'RSA-OAEP',
                modulusLength: 2048,
                publicExponent: new Uint8Array([0x01, 0x00, 0x01]),
                hash: { name: 'SHA-256' },
            },
            {
                name: 'AES-GCM',
                length: 256,
            },
            false,
            ['encrypt', 'decrypt'],
        );
    }

    public static async ExportRawKey(aesRawKey: ICryptoKey): Promise<string> {
        return Buffer.from(await subtle.exportKey('raw', aesRawKey)).toString('base64');
    }

    public static async ImportAesKey(base64ExportedAesKey: string): Promise<ICryptoKey> {
        return <ICryptoKey>(await (<any>subtle).importKey(
            'raw',
            Buffer.from(base64ExportedAesKey, 'base64'),
            'AES-GCM',
            true,
            ['encrypt', 'decrypt'],
        ));
    }

    public static async ExportPublicKey(key: ICryptoKey): Promise<string> {
        let publicKey = Buffer.from(JSON.stringify(await subtle.exportKey('jwk', key))).toString('base64');

        return publicKey;
    }

    public static async ExportPrivateKey(privateKey: ICryptoKey): Promise<string> {
        return Buffer.from(await subtle.exportKey('pkcs8', privateKey)).toString('base64');
    }

    public static async ImportPrivateKey(privateKey: string): Promise<ICryptoKey> {
        return <ICryptoKey>(await (<any>subtle).importKey(
            'pkcs8',
            Buffer.from(privateKey, 'base64'),
            {
                name: 'RSA-OAEP',
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: 'SHA-256',
            },
            true,
            ['unwrapKey'],
        ));
    }

    public static async ImportPublicKey(publicKey: string): Promise<ICryptoKey> {
        return <ICryptoKey>(await (<any>subtle).importKey(
            'jwk',
            JSON.parse(Buffer.from(publicKey, 'base64').toString()),
            {
                name: 'RSA-OAEP',
                modulusLength: 4096,
                publicExponent: new Uint8Array([1, 0, 1]),
                hash: 'SHA-256',
            },
            true,
            ['wrapKey'],
        ));
    }

    public static async Sha256(arrayBuffer: ArrayBuffer): Promise<string> {
        let sha256Buffer = await subtle.digest('SHA-256', arrayBuffer);

        return this.ArrayBufferToHex(sha256Buffer);
    }

    public static async Sha1(arrayBuffer: ArrayBuffer): Promise<string> {
        let sha256Buffer = await subtle.digest('SHA-1', arrayBuffer);

        return this.ArrayBufferToHex(sha256Buffer);
    }

    private static ArrayBufferToHex(arrayBuffer: ArrayBuffer): string {
        return Buffer.from(arrayBuffer).toString('hex');
    }
}
