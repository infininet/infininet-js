import ICryptographer from './../../ext/infininet/lib/crypto/ICryptographer';
import { ICryptoKey } from './../../ext/infininet/lib/crypto/ISubtle';
import crypto from 'crypto';

let subtle = (<any>crypto).webcrypto.subtle;

export default class SubtleSymmetric implements ICryptographer {
    private aesKey: ICryptoKey;
    private iv: ArrayBuffer;

    public constructor(aesKey: ICryptoKey, iv: ArrayBuffer) {
        this.aesKey = aesKey;
        this.iv = iv;
    }

    public async decrypt(base64Payload: string): Promise<string> {
        return Buffer.from(await subtle.decrypt(
            {
                name: 'AES-GCM',
                iv: this.iv,
            },
            this.aesKey,
            Buffer.from(base64Payload, 'base64'),
        )).toString('utf8');
    }

    public async encrypt(plainUtf8: string): Promise<string> {
        return Buffer.from(await subtle.encrypt(
            {
                name: 'AES-GCM',
                iv: this.iv,
            },
            this.aesKey,
            plainUtf8,
        )).toString('base64');
    }
}
