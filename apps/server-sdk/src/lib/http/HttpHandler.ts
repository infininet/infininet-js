import { IRequestHandler } from './../../ext/infininet/lib/ICommunicator';
import { IHttpHandler } from './../../ext/infininet/lib/http/IHttpHandler';

import { IIncomingRequest } from './../../ext/infininet/lib/communicator/incomming/IIncomingRequest';
import { IRequest } from './../../ext/infininet/lib/communicator//remote/http/IRequest';
import { IResponse } from './../../ext/infininet/lib/communicator/remote/http/IResponse';
import { ProxyResponse } from './ProxyResponse';

import http, { IncomingMessage } from 'http';


export class HttpHandler implements IHttpHandler {

    private isEnabled: boolean = false;
    private proxyHost: string | null = null;
    private proxyPort: number | null = null;

    getHandler(): IRequestHandler {
        return (req: IIncomingRequest): void => {
            try {
                if (!this.isEnabled) {
                    req.respond(new Error('No Http Proxy available'));
                    return;
                }
                let httpRequest = <IRequest>req.getParams();
                let headers = {};
                if (httpRequest.headers) {
                    headers = httpRequest.headers;
                }
                let hasBody = !['GET', 'DELETE'].includes(httpRequest.method.toUpperCase());
                if (hasBody) {
                    if (!httpRequest.body) {
                        throw new Error('Request Body required');
                    }
                    headers['Content-Length'] = httpRequest.body.length;
                }

                let proxyOptions = {
                    method: httpRequest.method,
                    hostname: this.proxyHost,
                    port: this.proxyPort,
                    path: httpRequest.path,
                    headers: headers,
                };

                let proxyReq = http.request(proxyOptions, async (rawRes: IncomingMessage) => {
                    let proxyRes = new ProxyResponse(rawRes);
                    console.log()
                    await proxyRes.evaluate();

                    req.respond(null, <IResponse>{
                        status: proxyRes.getStatus(),
                        headers: proxyRes.getHeaders(),
                        body: proxyRes.getBody(),
                    });
                });

                proxyReq.on('error', (err) => {
                    req.respond(err);
                });

                if (hasBody) {
                    proxyReq.write(httpRequest.body, () => {
                        proxyReq.end();
                    });
                } else {
                    proxyReq.end();
                }
            } catch (err) {
                req.respond(err);
            }
        };
    }

    public setUpProxyEndpoint(host: string, port: number): void {
        this.isEnabled = true;
        this.proxyHost = host;
        this.proxyPort = port;

        console.log(`InfiniNet proxy enabled, endpoint ${host}:${port}`)
    }
}
