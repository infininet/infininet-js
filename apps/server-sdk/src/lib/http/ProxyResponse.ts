import { IncomingMessage, IncomingHttpHeaders } from 'http';

export interface IResponseHeaders extends IncomingHttpHeaders {}

export class ProxyResponse {
    protected headers: IncomingHttpHeaders;
    protected response: IncomingMessage;

    protected status?: number;
    protected body?: string;

    public constructor(response: IncomingMessage) {
        this.response = response;
        this.headers = response.headers;
        this.status = response.statusCode;
    }

    public getHeaders(): IResponseHeaders {
        return this.headers;
    }

    public getStatus(): number {
        return <number> this.status;
    }

    public getBody(): string {
        return <string> this.body;
    }

    public async evaluate(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                let chunks: Array<string> = [];
                this.response.on('data', (chunk) => {
                    chunks.push(chunk.toString());
                });
                this.response.on('end', () => {
                    this.body = chunks.join('');
                    resolve();
                });
            } catch (err) {
                reject(err);
            }
        });
    }
}
