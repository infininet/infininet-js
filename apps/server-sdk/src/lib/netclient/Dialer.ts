import IDialer from './../../ext/infininet/lib/IDialer';
import ISocket from './../../ext/infininet/lib/stream/ISocket';

import net from 'net';

import Socket from './Socket';

export default class Dialer implements IDialer {
    public async dial(host: string, port?: number): Promise<ISocket> {
        return new Promise((resolve, reject) => {
            try {
                if (!port || isNaN(Number(port))) {
                    throw new Error('Port required');
                }

                let socket = new net.Socket();
                socket.connect(port, host, () => {
                    resolve(new Socket(socket));
                });

                socket.on('error', (err: Error) => {
                    reject(err);
                });
            } catch (err) {
                reject(err);
            }
        });
    }
}
