import ISocket from './../../ext/infininet/lib/stream/ISocket';
import net from 'net';

export default class Socket implements ISocket {
    protected netSocket: net.Socket;

    public constructor(netSocket: net.Socket) {
        this.netSocket = netSocket;
    }

    public async close(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.netSocket.end(() => {
                    resolve();
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    public async write(message: string): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.netSocket.write(message, (err?: Error) => {
                    if (err) {
                        reject(err);
                    }

                    resolve();
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    public onData(callback: (data: string) => void): void {
        this.netSocket.on('data', data => callback(data.toString()));
    }

    public onceData(callback: (data: string) => void): void {
        this.netSocket.once('data', data => callback(data.toString()));
    }

    public onDisconnect(callback: () => void): void {
        this.netSocket.on('end', callback);
    }
}
