import IEncoder from './../../ext/infininet/core/encoding/IEncoder';
import MD5 from './../../ext/infininet/core/encoding/MD5';

export default class Encoder implements IEncoder {
    public md5(source: string): string {
        return MD5(source);
    }

    public toBase64(utf8: string): string {
        return Buffer.from(utf8, 'utf8').toString('base64');
    }

    public fromBase64(base64: string): string {
        return Buffer.from(base64, 'base64').toString('utf8');
    }

    public toBuffer(source: string): ArrayBuffer {
        return Buffer.from(source);
    }

    public serialize<T>(obj: T): string {
        return this.toBase64(JSON.stringify(obj));
    }

    public unserialize<T>(serialized: string): T {
        return JSON.parse(this.fromBase64(serialized)) as T;
    }
}
