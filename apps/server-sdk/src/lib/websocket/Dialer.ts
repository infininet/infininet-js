import IDialer from './../../ext/infininet/lib/IDialer';
import ISocket from './../../ext/infininet/lib/stream/ISocket';

import ws from 'ws';

import Socket from './Socket';

export default class Dialer implements IDialer {
    public async dial(host: string, port?: number, secure?: boolean): Promise<ISocket> {
        return new Promise((resolve, reject) => {
            try {
                let path = [secure === false ? 'ws' : 'wss', '://', host];
                if (port) {
                    path.push(':', port.toFixed(0));
                }
                console.log('Connecting to Infininode at', path.join(''));
                let websocket = new ws(path.join(''));
                websocket.on('open', () => {
                    console.log('WS CONNECTION OPEN');
                    resolve(new Socket(websocket));
                });

                websocket.on('error', (err: Error) => {
                    reject(err);
                });
            } catch (err) {
                reject(err);
            }
        });
    }
}
