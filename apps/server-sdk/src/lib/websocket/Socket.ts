import ISocket from './../../ext/infininet/lib/stream/ISocket';
import ws from 'ws';

export default class Socket implements ISocket {
    protected websocket: ws;

    public constructor(websocket: ws) {
        this.websocket = websocket;

        this.websocket.on('error', (err: Error) => {
            console.log(err);
        });
    }

    public async close(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.websocket.close();
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    public async write(message: string): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.websocket.send(message, (err?: Error) => {
                    if (err) {
                        reject(err);
                    }

                    resolve();
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    public onData(callback: (data: string) => void): void {
        this.websocket.on('message', (data) => {
            callback(data.toString());
        });
    }

    public onceData(callback: (data: string) => void): void {
        this.websocket.once('message', data => callback(data.toString()));
    }

    public onDisconnect(callback: () => void): void {
        this.websocket.on('close', callback);
    }
}
