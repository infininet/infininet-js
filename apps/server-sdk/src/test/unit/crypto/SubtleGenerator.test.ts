import SubtleGenerator from './../../../lib/crypto/SubtleGenerator';

describe('Testing encryption logic', () => {
    it('Should generate RSA keys for encrypt and decrypt', async () => {
        let cryptokey = await SubtleGenerator.GenerateRsaKeyPair();

        expect(cryptokey.publicKey.type).toBe('public');
        expect(cryptokey.publicKey.algorithm.name).toBe('RSA-OAEP');
        expect(cryptokey.publicKey.algorithm.modulusLength).toBe(4096);
        expect(cryptokey.publicKey.algorithm.hash?.name).toBe('SHA-256');
        expect(cryptokey.publicKey.extractable).toBeTruthy();
        expect(cryptokey.publicKey.usages.length).toBe(1);
        expect(cryptokey.publicKey.usages).toContain('encrypt');

        expect(cryptokey.privateKey.type).toBe('private');
        expect(cryptokey.privateKey.algorithm.name).toBe('RSA-OAEP');
        expect(cryptokey.privateKey.algorithm.modulusLength).toBe(4096);
        expect(cryptokey.privateKey.algorithm.hash?.name).toBe('SHA-256');
        expect(cryptokey.privateKey.extractable).toBeTruthy();
        expect(cryptokey.privateKey.usages.length).toBe(1);
        expect(cryptokey.privateKey.usages).toContain('decrypt');
    });

    it('Should generate RSA keys for wrap and unwrap', async () => {
        let cryptokey = await SubtleGenerator.GenerateRsaWrapKeyPair();

        expect(cryptokey.publicKey.type).toBe('public');
        expect(cryptokey.publicKey.algorithm.name).toBe('RSA-OAEP');
        expect(cryptokey.publicKey.algorithm.modulusLength).toBe(4096);
        expect(cryptokey.publicKey.algorithm.hash?.name).toBe('SHA-256');
        expect(cryptokey.publicKey.extractable).toBeTruthy();
        expect(cryptokey.publicKey.usages.length).toBe(1);
        expect(cryptokey.publicKey.usages).toContain('wrapKey');

        expect(cryptokey.privateKey.type).toBe('private');
        expect(cryptokey.privateKey.algorithm.name).toBe('RSA-OAEP');
        expect(cryptokey.privateKey.algorithm.modulusLength).toBe(4096);
        expect(cryptokey.privateKey.algorithm.hash?.name).toBe('SHA-256');
        expect(cryptokey.privateKey.extractable).toBeTruthy();
        expect(cryptokey.privateKey.usages.length).toBe(1);
        expect(cryptokey.privateKey.usages).toContain('unwrapKey');
    });

    it('Should generate a valid AES-GCM 256 key for encryption and decryption', async () => {
        let aesKey = await SubtleGenerator.GenerateAesKey();

        expect(aesKey.type).toBe('secret');
        expect(aesKey.algorithm.name).toBe('AES-GCM');
        expect(aesKey.algorithm.length).toBe(256);
        expect(aesKey.extractable).toBeTruthy();
        expect(aesKey.usages.length).toBe(2);
        expect(aesKey.usages).toContain('encrypt');
        expect(aesKey.usages).toContain('decrypt');
    });

    it('Should wrap and unwrap a AES Key with a RSA KeyPair', async () => {
        let rsaKeyPair = await SubtleGenerator.GenerateRsaWrapKeyPair();
        let aesKey = await SubtleGenerator.GenerateAesKey();

        let wrappedKey = await SubtleGenerator.WrapAesKey(aesKey, rsaKeyPair.publicKey);

        expect(wrappedKey.length).toBe(684);

        let unwrappedKey = await SubtleGenerator.UnwrapAesKey(wrappedKey, rsaKeyPair.privateKey);

        expect(unwrappedKey.type).toBe('secret');
        expect(unwrappedKey.algorithm.name).toBe('AES-GCM');
        expect(unwrappedKey.algorithm.length).toBe(256);
        expect(unwrappedKey.extractable).toBeFalsy();
        expect(aesKey.usages.length).toBe(2);
        expect(unwrappedKey.usages).toContain('encrypt');
        expect(unwrappedKey.usages).toContain('decrypt');
    });
});
