import SubtleGenerator from './../../../lib/crypto/SubtleGenerator';
import SubtleSymmetric from './../../../lib/crypto/SubtleSymmentric';

describe('Testing encryption logic', () => {
    it('Should encrypt with imported symmeric key', async () => {
        let aesKey = await SubtleGenerator.ImportAesKey('s2UMdDmVWBGtvH1oQa8OmG4VY5dzSfRV+GgOtOR/hhs=');
        let iv = Buffer.from('uniquenoncewithminlength');
        let cryptographer = new SubtleSymmetric(aesKey, iv);

        let cipherText = await cryptographer.encrypt('plain utf8 with special chars <>"äöüß');

        expect(cipherText).toBe('WvIy7C4CBxIdsaIYnuNFpWDJI25C9pdSuAtC9s3rJD8CJeY+gms3Bb9LdLCZL8iJBlnZtIGeqXgH');
    });

    it('Should decrypt with imported symmeric key', async () => {
        let aesKey = await SubtleGenerator.ImportAesKey('s2UMdDmVWBGtvH1oQa8OmG4VY5dzSfRV+GgOtOR/hhs=');
        let iv = Buffer.from('uniquenoncewithminlength');
        let cryptographer = new SubtleSymmetric(aesKey, iv);

        let plainText = await cryptographer.decrypt('WvIy7C4CBxIdsaIYnuNFpWDJI25C9pdSuAtC9s3rJD8CJeY+gms3Bb9LdLCZL8iJBlnZtIGeqXgH');

        expect(plainText).toBe('plain utf8 with special chars <>"äöüß');
    });

    it('Should fail decryption with wrong nonce', async () => {
        let aesKey = await SubtleGenerator.ImportAesKey('s2UMdDmVWBGtvH1oQa8OmG4VY5dzSfRV+GgOtOR/hhs=');
        let iv = Buffer.from('uniquenoncewithminlengthbutnottherightone');
        let cryptographer = new SubtleSymmetric(aesKey, iv);

        try {
            await cryptographer.decrypt('WvIy7C4CBxIdsaIYnuNFpWDJI25C9pdSuAtC9s3rJD8CJeY+gms3Bb9LdLCZL8iJBlnZtIGeqXgH');

            fail(new Error('Nonce was ignored'));
        } catch (err) {
            expect(err.message).toBe('Cipher job failed');
        }
    });
});
