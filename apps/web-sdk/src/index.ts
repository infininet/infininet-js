export * from './ext/infininet/lib/InfiniClient';
export * from './ext/infininet/lib/communicator/incomming/IIncomingRequest';

import { InfiniClient } from './ext/infininet/lib/InfiniClient';
import WebClientContainer from './lib/container/WebClientContainer';

let client = new InfiniClient((new WebClientContainer()).getConnector());

let exp = {
    getClient(): InfiniClient {
        return client;
    },
    InfiniClient,
};

export default exp;
