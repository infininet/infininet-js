import ICryptographer from './../../ext/infininet/lib/crypto/ICryptographer';
import IKeyGenerator, { IKeyPair } from './../../ext/infininet/lib/crypto/IKeyGenerator';
import { ICryptoKey } from './../../ext/infininet/lib/crypto/ISubtle';
import SubtleGenerator from './SubtleGenerator';
import SubtleSymmetric from './SubtleSymmentric';

import * as buffer from 'buffer';
const Buffer = buffer.Buffer;

export default class KeyGenerator implements IKeyGenerator {
    public async generateKeyPair(): Promise<IKeyPair> {
        let cryptoKeys = await SubtleGenerator.GenerateRsaWrapKeyPair();

        let privateKeyExp = await SubtleGenerator.ExportPrivateKey(cryptoKeys.privateKey);
        let publicKeyExp = await SubtleGenerator.ExportPublicKey(cryptoKeys.publicKey);

        return {
            privateKey: cryptoKeys.privateKey,
            publicKey: cryptoKeys.publicKey,
            privateKeyExp: privateKeyExp,
            publicKeyExp: publicKeyExp,
            sha256Address: await SubtleGenerator.Sha256(Buffer.from(publicKeyExp, 'base64')),
        };
    }

    public getSymmetricCryptographer(aesKey: ICryptoKey, iv: ArrayBuffer): ICryptographer {
        return new SubtleSymmetric(aesKey, iv);
    }

    public async generateAesKey(): Promise<ICryptoKey> {
        return SubtleGenerator.GenerateAesKey();
    }

    public async importPrivateKey(privateKey: string): Promise<ICryptoKey> {
        return SubtleGenerator.ImportPrivateKey(privateKey);
    }

    public async importPublicKey(publicKey: string): Promise<ICryptoKey> {
        return SubtleGenerator.ImportPublicKey(publicKey);
    }

    public async importAesKey(aesKey: string): Promise<ICryptoKey> {
        return SubtleGenerator.ImportAesKey(aesKey);
    }

    public async exportAesKey(aesKey: ICryptoKey): Promise<string> {
        return SubtleGenerator.ExportRawKey(aesKey);
    }

    public async wrapAesKey(aesKey: ICryptoKey, publicKey: ICryptoKey): Promise<string> {
        return SubtleGenerator.WrapAesKey(aesKey, publicKey);
    }

    public async unwrapAesKey(aesKey: string, privateKey: ICryptoKey): Promise<ICryptoKey> {
        return SubtleGenerator.UnwrapAesKey(aesKey, privateKey);
    }
}
