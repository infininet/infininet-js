import * as buffer from 'buffer';
const Buffer = buffer.Buffer;

import IEncoder from './../../ext/infininet/core/encoding/IEncoder';
import MD5 from './../../ext/infininet/core/encoding/MD5';

export default class Encoder implements IEncoder {
    public md5(source: string): string {
        return MD5(source);
    }

    public toBase64(utf8: string): string {
        return base64ArrayBuffer(Buffer.from(utf8, 'utf8'));
    }

    public fromBase64(base64: string): string {
        return String.fromCharCode.apply(null, new Uint16Array(Buffer.from(base64, 'base64')));
    }

    public toBuffer(source: string): ArrayBuffer {
        return Buffer.from(source);
    }

    public serialize<T>(obj: T): string {
        return this.toBase64(JSON.stringify(obj));
    }

    public unserialize<T>(serialized: string): T {
        return JSON.parse(this.fromBase64(serialized)) as T;
    }
}

function base64ArrayBuffer(arrayBuffer: ArrayBuffer): string {
    var base64    = ''
    var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'

    var bytes         = new Uint8Array(arrayBuffer)
    var byteLength    = bytes.byteLength
    var byteRemainder = byteLength % 3
    var mainLength    = byteLength - byteRemainder

    var a, b, c, d
    var chunk

    // Main loop deals with bytes in chunks of 3
    for (var i = 0; i < mainLength; i = i + 3) {
      // Combine the three bytes into a single integer
      chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2]

      // Use bitmasks to extract 6-bit segments from the triplet
      a = (chunk & 16515072) >> 18 // 16515072 = (2^6 - 1) << 18
      b = (chunk & 258048)   >> 12 // 258048   = (2^6 - 1) << 12
      c = (chunk & 4032)     >>  6 // 4032     = (2^6 - 1) << 6
      d = chunk & 63               // 63       = 2^6 - 1

      // Convert the raw binary segments to the appropriate ASCII encoding
      base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d]
    }

    // Deal with the remaining bytes and padding
    if (byteRemainder == 1) {
      chunk = bytes[mainLength]

      a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2

      // Set the 4 least significant bits to zero
      b = (chunk & 3)   << 4 // 3   = 2^2 - 1

      base64 += encodings[a] + encodings[b] + '=='
    } else if (byteRemainder == 2) {
      chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1]

      a = (chunk & 64512) >> 10 // 64512 = (2^6 - 1) << 10
      b = (chunk & 1008)  >>  4 // 1008  = (2^6 - 1) << 4

      // Set the 2 least significant bits to zero
      c = (chunk & 15)    <<  2 // 15    = 2^4 - 1

      base64 += encodings[a] + encodings[b] + encodings[c] + '='
    }

    return base64
  }