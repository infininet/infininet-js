import Socket from './Socket';
import IDialer from './../../ext/infininet/lib/IDialer';
import ISocket from './../../ext/infininet/lib/stream/ISocket';
const WebSocket = window.WebSocket;

export default class Dialer implements IDialer {
    public async dial(host: string, port?: number, secure?: boolean): Promise<ISocket> {
        return new Promise((resolve, reject) => {
            try {
                let path = [secure === false ? 'ws' : 'wss', '://', host];
                if (port) {
                    path.push(':', port.toFixed(0));
                }
                let websocket = new WebSocket(path.join(''), 'echo-protocol');
                websocket.onopen = (ev: Event) => {
                    console.log('WS CONNECTION OPEN');
                    resolve(new Socket(websocket));
                };

                websocket.onerror = (ev: Event) => {
                    console.log(ev);
                    reject(new Error("Websocket error"));
                };
            } catch (err) {
                reject(err);
            }
        });
    }
}