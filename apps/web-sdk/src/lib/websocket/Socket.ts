import ISocket from './../../ext/infininet/lib/stream/ISocket';

const WebSocket = window.WebSocket;
const Event = window.Event;

export default class Socket implements ISocket {
    protected websocket: WebSocket;

    public constructor(websocket: WebSocket) {
        this.websocket = websocket;

        this.websocket.onerror = (ev: Event) => {
            console.log(ev);
        };
    }

    public async close(): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.websocket.close();
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    public async write(message: string): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.websocket.send(message);
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    public onData(callback: (data: string) => void): void {
        this.websocket.onmessage = (data) => {
            console.log(data);
            callback(data.data);
        };
    }

    public onceData(callback: (data: string) => void): void {
        let oneTimeListener = (data: MessageEvent) => {
            this.websocket.removeEventListener('message', oneTimeListener);
            callback(data.data);
        };

        this.websocket.addEventListener('message', oneTimeListener);
    }

    public onDisconnect(callback: () => void): void {
        this.websocket.onclose = callback;
    }
}
