import { IIncomingRequest } from './communicator/incomming/IIncomingRequest';
import IncomingRequest from './communicator/incomming/IncomingRequest';
import ITransfer from './communicator/ITransfer';
import IOutgoingRequest from './communicator/outgoing/IOutgoingRequest';
import IAction from './communicator/remote/IAction';
import IMeta from './communicator/remote/IMeta';
import IRemoteData from './communicator/remote/IRemoteData';
import ICryptographer from './crypto/ICryptographer';
import IKeyGenerator, { IKeyPair } from './crypto/IKeyGenerator';
import IConnection from './dialer/IConnection';
import ICommunicator, { IRequestHandler } from './ICommunicator';
import IEncoder from './../core/encoding/IEncoder';
import { EventEmitter } from 'events';

interface IIncomingMeta {
    requestId: string;
    from: string;
    to: string;
}

interface IIncomingErrorData {
    kind: 'ERR';
    requestId: string;
    errorCode: string;
    errorMessage: string;
}

interface IIncomingTransmission {
    kind: 'MSG' | 'RES';
    meta: IIncomingMeta;
    payload: string;
}

type IAwaitResponseCallback = (meta: IIncomingMeta | IIncomingErrorData, err: Error | null,  data?: unknown) => void;

export default class Communicator extends EventEmitter implements ICommunicator {
    private requestCounter: number = 0;

    private connection?: IConnection;
    private actionListener: Set<IRequestHandler> = new Set<IRequestHandler>();
    private requests: Map<string, IAwaitResponseCallback> = new Map<string, IAwaitResponseCallback>();

    private remoteData: Array<IRemoteData> = [];

    private encoder: IEncoder;
    private keyGen: IKeyGenerator;
    private keyPair?: IKeyPair;

    public constructor(keyGen: IKeyGenerator, encoder: IEncoder) {
        super();
        this.keyGen = keyGen;
        this.encoder = encoder;
    }

    public isConnected(): boolean {
        return !!this.connection;
    }

    public async getKeyPair(): Promise<IKeyPair> {
        if (!this.keyPair) {
            this.keyPair = await this.keyGen.generateKeyPair();
        }

        return this.keyPair;
    }

    public async getAddress(): Promise<string> {
        return (await this.getKeyPair()).sha256Address;
    }

    public getRemote(identifier: string): IRemoteData | undefined {
        for (let remoteData of this.remoteData) {
            if (remoteData.app === identifier || remoteData.instance === identifier) {
                return remoteData;
            }
        }

        return undefined;
    }

    protected parseIncoming(data: string): IIncomingTransmission | IIncomingErrorData {
        let parts = data.split('.');
        let kind = parts.shift();
        if (kind === 'MSG' || kind === 'RES') {
            let meta = {
                requestId: parts.shift() as string,
                from: parts.shift() as string,
                to: parts.shift() as string,
            };

            return {
                kind: kind,
                meta: meta,
                payload: parts.shift() as string,
            };
        } else if (kind === 'ERR') {
            return {
                kind: kind,
                requestId: parts.shift() as string,
                errorCode: parts.shift() as string,
                errorMessage: this.encoder.fromBase64(parts.shift() as string),
            };
        }

        throw new Error('Unplausible Transmission');
    }

    public setConnection(connection: IConnection): void {
        if (this.isConnected()) {
            throw new Error("Already connected");
        }

        this.connection = connection;
        this.connection.once('end', () => {
            this.emit('offline');
            delete this.connection;
            this.remoteData = [];
        });

        this.connection.onMessage((data: string) => {
            try {
                let incoming = this.parseIncoming(data);
                if (incoming.kind === 'ERR') {
                    incoming = <IIncomingErrorData>incoming;
                    let awaiter = this.spliceRequestAwaiter(incoming.requestId);
                    if (awaiter) {
                        awaiter(incoming, new Error(incoming.errorMessage));
                    } else {
                        console.log('UNHANDLED ERROR RESPONSE', incoming);
                    }

                    return;
                }

                incoming = <IIncomingTransmission>incoming;
                if (incoming.kind === 'RES') {
                    void this.onIncomingRES(incoming.meta, incoming.payload);
                } else if (incoming.kind === 'MSG') {
                    void this.onIncomingMSG(incoming.meta, incoming.payload);
                } else {
                    console.log('GOT UNSUPPORTED', incoming);
                }
            } catch (err) {
                console.log(err, data);
            }
        });

        this.emit('online');
    }

    protected async onIncomingRES(meta: IIncomingMeta, payload: string): Promise<void> {
        try {
            let awaiter = this.spliceRequestAwaiter(meta.requestId);
            if (awaiter) {
                awaiter(meta, null, payload);
            } else {
                console.log('UNHANDLED RESPONSE', meta, payload);
            }
        } catch (err) {
            console.log(err);
        }
    }

    protected async onIncomingMSG(meta: IIncomingMeta, payload: string): Promise<void> {
        if (payload.startsWith('HELLO')) {
            void this.respondHello(meta, payload);

            return;
        }

        // ENCRYPTED from here on
        let remote = this.getRemote(meta.from);
        if (!remote) {
            console.log('GOT MESSAGE FROM UNKNOWN', meta);

            return;
        }

        let decrypted = (await this.retrievePayload(
            payload,
            this.keyGen.getSymmetricCryptographer(remote.aesKey, this.encoder.toBuffer(meta.requestId)),
        )).split('_');

        void this.handleIncomingRequest(remote, meta, decrypted);
    }

    protected async handleIncomingRequest(remote: IRemoteData, meta: IIncomingMeta, payload: Array<string>): Promise<void> {
        try {
            let params;
            try {
                params = payload[1] !== undefined ? this.encoder.unserialize(payload[1]) : {};
            } catch (err) {
                params = {};
            }

            if (payload[0] === 'ACTION') {
                let action = <IAction>params;
                let request = new IncomingRequest(action.action, action.params, remote, async (err: Error | null, data: Record<string, unknown>) => {
                    let payload;
                    if (err) {
                        payload = JSON.stringify({
                            success: false,
                            errorMessage: err.message,
                        });
                    } else {
                        payload = JSON.stringify({
                            success: true,
                            data: data || {},
                        });
                    }

                    let conn = this.getConnection();

                    let encrypted = await this.keyGen.getSymmetricCryptographer(
                        remote.aesKey,
                        this.encoder.toBuffer(meta.requestId),
                    ).encrypt(payload);

                    await conn.send(`RES.${meta.requestId}.${(await this.getAddress())}.${remote.instance}.${encrypted};`);
                });

                this.dispatchAction(request);
            } else {
                throw new Error('Unsupported Request');
            }
        } catch (err) {
            console.log(err);
        }
    }

    public async sendRequest(endpoint: string, request: IOutgoingRequest): Promise<string> {
        let remote = this.getRemote(endpoint);
        if (!remote) {
            throw new Error(`Unknown remote '${endpoint}'`);
        }

        let instance = remote.instance;

        return new Promise(async (resolve, reject) => {
            try {
                let { payload } = await this.transfer({
                    kind: 'MSG',
                    sender: await this.getAddress(),
                    recipient: instance,
                    encrypt: true,
                    payload: ['ACTION', this.encoder.serialize(request.toJSON())].join('_'),
                });

                resolve(await payload);
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }

    public async connectRemote(endpoint: string): Promise<IRemoteData> {
        return new Promise(async (resolve, reject) => {
            try {
                let keyPair = await this.getKeyPair();

                let { meta, payload } = await this.transfer({
                    kind: 'MSG',
                    sender: keyPair.sha256Address,
                    recipient: endpoint,
                    encrypt: false,
                    payload: ['HELLO', this.encoder.toBase64(keyPair.publicKeyExp)].join('_'),
                });

                let remote = <IRemoteData>(await this.getMeta(meta.from));
                remote.aesKey = await this.keyGen.unwrapAesKey(payload, (await this.getKeyPair()).privateKey);

                this.remoteData.push(remote);

                resolve(remote);
            } catch (err) {
                reject(err);
            }
        });
    }

    protected async respondHello(meta: IIncomingMeta, helloPayload: string): Promise<void> {
        try {
            let hello = helloPayload.split('_');
            if (hello.length !== 2) {
                console.log('GOT INVALID HELLO', hello);

                return;
            }
            let publicKey = await this.keyGen.importPublicKey(this.encoder.fromBase64(hello[1]));

            let remote = await this.getRemote(meta.from);
            if (!remote) {
                remote = <IRemoteData>(await this.getMeta(meta.from));
                remote.publicKey = publicKey;
                remote.aesKey = await this.keyGen.generateAesKey();

                this.remoteData.push(remote);
            }

            let myKeyPair = await this.getKeyPair();
            let conn = this.getConnection();

            let payload = await this.keyGen.wrapAesKey(remote.aesKey, publicKey);
            await conn.send(`RES.${meta.requestId}.${myKeyPair.sha256Address}.${remote.instance}.${payload};`);
        } catch (err) {
            console.log(err);
        }
    }

    protected spliceRequestAwaiter(requestId: string): IAwaitResponseCallback | null {
        let callback = this.requests.get(requestId);
        if (callback) {
            this.requests.delete(requestId);

            return callback;
        }

        return null;
    }

    protected getConnection(): IConnection {
        if (!this.connection) {
            throw new Error('Not connected');
        }

        return this.connection;
    }

    public onAction(callback: IRequestHandler): void {
        this.actionListener.add(callback);
    }

    protected dispatchAction(actionRequest: IIncomingRequest): void {
        this.actionListener.forEach(callback => callback(actionRequest));
    }

    protected async generateTransferString(requestId: string, transfer: ITransfer): Promise<string> {
        return [transfer.kind, requestId, transfer.sender, transfer.recipient, transfer.payload].join('.') + ';';
    }

    protected async retrievePayload(payload: string, cryptographer?: ICryptographer): Promise<string> {
        if (!cryptographer) {
            return this.encoder.fromBase64(payload);
        }

        return cryptographer.decrypt(payload);
    }

    protected async transfer(transfer: ITransfer): Promise<{
        meta: IIncomingMeta;
        payload: string;
    }> {
        return new Promise(async (resolve, reject) => {
            let requestId;
            let start = Date.now();
            try {
                console.log('beginn tranfer')
                let conn = this.getConnection();
                requestId = this.encoder.md5(transfer.payload + Math.random());
                let cryptographer: ICryptographer;
                if (transfer.encrypt) {
                    console.log('encrypting tranfer')
                    let remote = await this.getRemote(transfer.recipient);
                    if (!remote) {
                        throw new Error(`Remote not found ${transfer.recipient}`);
                    }

                    cryptographer = this.keyGen.getSymmetricCryptographer(remote.aesKey, this.encoder.toBuffer(requestId));
                    transfer.payload = await cryptographer.encrypt(transfer.payload);
                    console.log('encrypted')
                }

                this.requests.set(requestId, async (meta: IIncomingMeta, err: Error | null, payload: string) => {
                    console.log(`[DURATION]`, (Date.now() - start).toFixed(2), 'ms');
                    if (err) {
                        reject(err);
                    }

                    if (transfer.encrypt) {
                        if (!cryptographer) {
                            throw new Error(`Cryptographer not initialized`);
                        }
                        payload = await cryptographer.decrypt(payload);
                    }

                    try {
                        resolve({ meta: meta, payload: payload });
                    } catch (err) {
                        reject(err);
                    }
                });

                console.log('sending')
                await conn.send(await this.generateTransferString(requestId, transfer));
                console.log('sent')
            } catch (err) {
                if (requestId) {
                    this.requests.delete(requestId);
                }
                reject(err);
            }
        });
    }

    public async getMeta(instance: string): Promise<IMeta> {
        return new Promise(async (resolve, reject) => {
            try {
                let myKeyPair = await this.getKeyPair();

                let { payload } = await this.transfer({
                    kind: 'INF',
                    sender: myKeyPair.sha256Address,
                    recipient: myKeyPair.sha256Address,
                    payload: instance,
                    encrypt: false,
                });

                resolve(<IMeta>JSON.parse(await this.retrievePayload(payload)));
            } catch (err) {
                reject(err);
            }
        });
    }
}
