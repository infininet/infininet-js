import { EventEmitter } from 'events';
import { IIncomingRequest } from './communicator/incomming/IIncomingRequest';
import IOutgoingRequest from './communicator/outgoing/IOutgoingRequest';
import IMeta from './communicator/remote/IMeta';
import IRemoteData from './communicator/remote/IRemoteData';
import IConnection from './dialer/IConnection';

export type IRequestHandler = (request: IIncomingRequest) => void;

export default interface ICommunicator extends EventEmitter {
    isConnected(): boolean;
    setConnection(connection: IConnection): void;
    onAction(callback: IRequestHandler): void;
    getMeta(instance: string): Promise<IMeta>;
    getRemote(identifier: string): IRemoteData | undefined;
    connectRemote(endpoint: string): Promise<IRemoteData>;
    getAddress(): Promise<string>;
    sendRequest(endpoint: string, data: IOutgoingRequest): Promise<string>;
}
