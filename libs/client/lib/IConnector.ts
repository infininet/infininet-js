import IRemote from './communicator/IRemote';
import ICommunicator, { IRequestHandler } from './ICommunicator';

export interface IConnectOptions {
    host: string;
    port?: number;
    secure?: boolean;
    version: string;
    app: string;
    environment: string;
    network: string;
    proxy?: {
        host: string,
        port: number,
    }
}

export default interface IConnector {
    connectNetwork(options: IConnectOptions): Promise<ICommunicator>;
    connectEndpoint(endpoint: string): Promise<IRemote>;
    assignActionHandler(action: string, callback: IRequestHandler): void;
    getCommunicator(): ICommunicator;
}
