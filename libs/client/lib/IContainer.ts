import IConnector from './IConnector';

export default interface IContainer {
    getConnector(): IConnector;
}
