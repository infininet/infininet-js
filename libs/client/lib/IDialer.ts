import ISocket from './stream/ISocket';

export default interface IDialer {
    dial(host: string, port?: number, secure?: boolean): Promise<ISocket>;
}
