import IRemote from './communicator/IRemote';
import { IRequestHandler } from './ICommunicator';
import IConnector, { IConnectOptions } from './IConnector';
import { IRestClient } from './http/IRestClient';
import { RestClient } from './http/RestClient';

export interface IActionCallback extends IRequestHandler {};

export class InfiniClient {
    private connector: IConnector;

    public constructor(connector: IConnector) {
        this.connector = connector;
    }

    protected getConnector(): IConnector {
        if (!this.connector) {
            throw new Error('Connector not set');
        }

        return this.connector;
    }

    public async init(options: IConnectOptions): Promise<void> {
        await this.connector.connectNetwork(options);
    }

    public on(action: string, callback: IRequestHandler): void {
        this.connector.assignActionHandler(action, callback);
    }

    public connect(endpoint: string): Promise<IRemote> {
        return this.connector.connectEndpoint(endpoint);
    }

    public isNetworkOnline(): boolean {
        return this.connector.getCommunicator().isConnected();
    }

    public onNetworkOnline(callback: () => void) {
        this.connector.getCommunicator().on('online', callback);
    }

    public onNetworkOffline(callback: () => void) {
        this.connector.getCommunicator().on('offline', callback);
    }

    public rest(endpoint: string): IRestClient {
        return new RestClient(this.connector, endpoint);
    }
}
