import { IIncomingRequest } from './incomming/IIncomingRequest';
import IRemote from './../communicator/IRemote';
import Remote from './../connector/Remote';
import IConnectionFactory from './../dialer/IConnectionFactory';
import ICommunicator, { IRequestHandler } from './../ICommunicator';
import { IHttpHandler } from './../http/IHttpHandler';
import IConnector, { IConnectOptions } from './../IConnector';
import IDialer from './../IDialer';

const reserverdActions = [
    'http'
];

export default class Connector implements IConnector {
    private connectionFactory: IConnectionFactory;
    private dialer: IDialer;
    private communicator: ICommunicator;
    private actionHandler: Map<string, IRequestHandler> = new Map<string, IRequestHandler>();
    private httpHandler?: IHttpHandler;

    private reconnectTimeout: NodeJS.Timeout;
    private nextReconnectIn: number = 100;

    public constructor(
        connectionFactory: IConnectionFactory,
        dialer: IDialer,
        communicator: ICommunicator,
        httpHandler?: IHttpHandler,
    ) {
        this.connectionFactory = connectionFactory;
        this.dialer = dialer;
        this.communicator = communicator;
        this.httpHandler = httpHandler;
    }

    public async connectNetwork(options: IConnectOptions): Promise<ICommunicator> {
        this.setUpProxy(options);

        await this.connect(options)
        this.communicator.onAction((request: IIncomingRequest) => {
            this.dispatchAction(request);
        });

        return this.communicator;
    }

    protected async connect(options: IConnectOptions): Promise<void> {
        try {
            if (this.communicator.isConnected()) {
                console.log('Connection already established, skipp connecting');
                return;
            }
            let connection = await this.connectionFactory.createNetworkConnection(
                await this.dialer.dial(options.host, options.port, options.secure),
                options,
                await this.communicator.getAddress()
            );

            this.communicator.setConnection(connection);
            connection.once('end', () => {
                console.log('Connection lost');
                this.nextReconnectIn = 100;
                this.reconnect(options);
            });
        } catch (err) {
            console.log(err);
            this.reconnect(options);
        }
    }

    protected async reconnect(options: IConnectOptions): Promise<void> {
        clearTimeout(this.reconnectTimeout);
        console.log(`Try to reconnect in ${(this.nextReconnectIn / 1000).toFixed(3)} Sec.`);
        this.reconnectTimeout = setTimeout(() => {
            this.connect(options);
        }, this.nextReconnectIn);

        // Double the time
        this.nextReconnectIn *= 2;
        if (this.nextReconnectIn > 10000) {
            // Max reconnection timeout
            this.nextReconnectIn = 10000;
        }
    }

    protected setUpProxy(options: IConnectOptions): void {
        if (options.proxy) {
            if (!this.httpHandler) {
                throw new Error('Cannot setup HttpHandler, handler is missing');
            }

            this.httpHandler.setUpProxyEndpoint(options.proxy.host, options.proxy.port);
            this.actionHandler.set('http', this.httpHandler.getHandler());
        }
    }

    public assignActionHandler(action: string, callback: IRequestHandler): void {
        if (this.actionHandler.has(action)) {
            throw new Error(`Action handler for '${action}' already assigned`);
        }
        if (reserverdActions.includes(action)) {
            throw new Error(`Cannot listen for reserved Action '${action}'`);
        }

        this.actionHandler.set(action, callback);
    }

    protected dispatchAction(actionRequest: IIncomingRequest): void {
        let handler = this.actionHandler.get(actionRequest.getAction());
        if (!handler) {
            actionRequest.respond(new Error('Action not supported'));

            return;
        }

        handler(actionRequest);
    }

    public async connectEndpoint(endpoint: string): Promise<IRemote> {
        let comm = this.getCommunicator();
        let remoteData = await comm.connectRemote(endpoint);

        return new Remote(comm, remoteData);
    }

    public getCommunicator(): ICommunicator {
        return this.communicator;
    }
}
