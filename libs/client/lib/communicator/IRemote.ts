import { IResponse } from './remote/http/IResponse';

export default interface IRemote {
    cmd(cmd: string, params?: Record<string, unknown>): Promise<unknown>;
    http(method: string, route: string, params?: Record<string, unknown>, body?: string): Promise<IResponse>;
}
