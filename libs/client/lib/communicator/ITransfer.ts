export default interface ITransfer {
    kind: 'MSG' | 'INF';
    sender: string;
    recipient: string;
    payload: string;
    encrypt: boolean;
}
