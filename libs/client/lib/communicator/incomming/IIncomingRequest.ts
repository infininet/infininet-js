export interface IIncomingRequest {
    getAction(): string;
    getParams(): Record<string, unknown>;
    respond(err: Error | null, data?: unknown): void;
}
