import { IIncomingRequest } from './../incomming/IIncomingRequest';
import IRemoteData from './../remote/IRemoteData';

export default class IncomingRequest implements IIncomingRequest {
    protected action: string;
    protected remoteData: IRemoteData;
    protected params: Record<string, unknown>;
    protected callback: (err: Error | null, data?: Record<string, unknown>) => void;

    public constructor(
        action: string,
        params: Record<string, unknown>,
        remoteData: IRemoteData,
        callback: (err: Error | null, data?: Record<string, unknown>) => void,
    ) {
        this.action = action;
        this.remoteData = remoteData;
        this.params = params;
        this.callback = callback;
    }

    public getAction(): string {
        return this.action;
    }

    public getParams(): Record<string, unknown> {
        return this.params;
    }

    public respond(err: Error | null, data?: Record<string, unknown>): void {
        this.callback(err, data);
    }
}
