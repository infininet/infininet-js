import IAction from './../remote/IAction';

export default interface IOutgoingRequest {
    toJSON(): IAction;
}
