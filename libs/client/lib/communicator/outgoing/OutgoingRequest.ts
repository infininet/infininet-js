import IOutgoingRequest from './../outgoing/IOutgoingRequest';
import IAction from './../remote/IAction';

export default class OutgoingRequest implements IOutgoingRequest {
    private action: IAction;

    public constructor(action: IAction) {
        this.action = action;
    }

    public toJSON(): IAction {
        return this.action;
    }
}
