export default interface IAction {
    action: string;
    params: Record<string, unknown>;
}
