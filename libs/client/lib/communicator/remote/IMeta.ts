export default interface IMeta {
    app: string;
    instance: string;
    network: string;
}
