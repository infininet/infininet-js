import IMeta from './IMeta';
import { ICryptoKey } from './../../crypto/ISubtle';

export default interface IRemoteData extends IMeta{
    publicKey: ICryptoKey;
    aesKey: ICryptoKey;
}
