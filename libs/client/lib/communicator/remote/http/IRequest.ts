export type IRequest = {
    method: string;
    path: string;
    headers?:Record<string, unknown>;
    body?: string;
}