export type IResponse = {
    status: number;
    headers: Record<string, unknown>;
    body: string;
}