import IConnection from './../dialer/IConnection';
import { IConnectOptions } from './../IConnector';
import ISocket from './../stream/ISocket';

export default interface IConnectionFactory {
    createNetworkConnection(socket: ISocket, options: IConnectOptions, address: string): Promise<IConnection>;
}
