import { IRequest } from './../communicator/remote/http/IRequest';
import { IResponse } from './../communicator/remote/http/IResponse';
import IRemote from './../communicator/IRemote';
import OutgoingRequest from './../communicator/outgoing/OutgoingRequest';
import IRemoteData from './../communicator/remote/IRemoteData';
import ICommunicator from './../ICommunicator';

export default class Remote implements IRemote {
    protected communicator: ICommunicator;
    protected remoteData: IRemoteData;

    public constructor(communicator: ICommunicator, remoteData: IRemoteData) {
        this.communicator = communicator;
        this.remoteData = remoteData;
    }

    protected async ensureConnection(): Promise<void> {
        if (!this.communicator.getRemote(this.remoteData.instance)){
            console.log('It seems that the connection was resetted, try to reconnect remote', this.remoteData.app);
            this.remoteData = await this.communicator.connectRemote(this.remoteData.app);
        }
    }

    public async http(method: string, route: string, params?: Record<string, unknown>, body?: string): Promise<IResponse> {
        return new Promise(async (resolve, reject) => {
            try {
                let path = route;
                if (params) {
                    path += '?' + Object.keys(params).map(key => key + '=' + encodeURIComponent(<string>params[key])).join('&');
                }
                let request = <IRequest>{
                    headers: {},
                    method: method,
                    path: path,
                    body: body
                };

                let response: any = await this.cmd('http', request);
                resolve(<IResponse>{
                    status: response.data.status,
                    headers: response.data.headers,
                    body: response.data.body,
                });
            } catch (err) {
                console.log(err);
                reject(err);
            }
        });

    }

    public async cmd(cmd: string, params?: Record<string, unknown>): Promise<unknown> {
        await this.ensureConnection();
        let res = await this.communicator.sendRequest(this.remoteData.instance, new OutgoingRequest({ action: cmd, params: params || {} }));
        try {
            return JSON.parse(res);
        } catch (err) {
            console.log(err);
            return res;
        }
    }
}
