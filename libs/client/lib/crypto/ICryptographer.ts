export default interface ICryptographer {
    encrypt(plainUtf8: string): Promise<string>;
    decrypt(encryptedBase64: string): Promise<string>;
}
