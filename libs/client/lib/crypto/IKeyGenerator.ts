import ICryptographer from './../crypto/ICryptographer';
import { ICryptoKey } from './../crypto/ISubtle';

export interface IKeyPair {
    privateKey: ICryptoKey;
    publicKey: ICryptoKey;
    privateKeyExp: string;
    publicKeyExp: string;
    sha256Address: string;
}

export default interface IKeyGenerator {
    generateKeyPair(): Promise<IKeyPair>;
    generateAesKey(): Promise<ICryptoKey>;
    importPrivateKey(privateKeyExp: string): Promise<ICryptoKey>;
    importPublicKey(privateKeyExp: string): Promise<ICryptoKey>;
    importAesKey(aesKeyExp: string): Promise<ICryptoKey>;
    exportAesKey(aesKey: ICryptoKey): Promise<string>;
    getSymmetricCryptographer(aesKey: ICryptoKey, iv: ArrayBuffer): ICryptographer;
    wrapAesKey(aesKey: ICryptoKey, publicKey: ICryptoKey): Promise<string>;
    unwrapAesKey(base64AesKey: string, privateKey: ICryptoKey): Promise<ICryptoKey>;
}
