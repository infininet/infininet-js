export enum AesAlgorithmTypes {
    'AES-CBC',
    'AES-CTR',
    'AES-GCM',
    'AES-KW'
}

export enum RsaAlgorithmTypes {
    'RSA-OAEP',
}

export interface IAlgorithm {

}

export interface IAesKeyGenParams extends IAlgorithm {
    name: string | AesAlgorithmTypes;
    length?: 128 | 192 | 256;
    modulusLength?: number;
    hash?: {
        name: 'SHA-256';
    };
}

export interface IRsaKeyGenParams extends IAlgorithm {
    name: 'RSA-OAEP';
    modulusLength: 4096;
    publicExponent: Uint8Array;
    hash: 'SHA-256';
}

export type KeyUsageType = 'encrypt' | 'decrypt' | 'sign' | 'verify' | 'deriveKey' | 'deriveBits' | 'wrapKey' | 'unwrapKey';

export type IKeyUsage = Array<KeyUsageType>;

export interface ICryptoKey {
    type: 'secret' | 'private' | 'public';
    extractable: boolean;
    algorithm: IAesKeyGenParams;
    usages: IKeyUsage;

}

export interface ICryptoKeyPair {
    privateKey: ICryptoKey;
    publicKey: ICryptoKey;
}

export interface IJwk {

}

export interface ISubtle {
    generateKey(algorithm: IRsaKeyGenParams, extractable: boolean, keyUsages: IKeyUsage): Promise<ICryptoKeyPair>;
    generateKey(algorithm: IAesKeyGenParams, extractable: boolean, keyUsages: IKeyUsage): Promise<ICryptoKey>;
    exportKey(format: 'jwk', key: ICryptoKey): Promise<IJwk>;
    exportKey(format: 'pkcs8', key: ICryptoKey): Promise<ArrayBuffer>;
    digest(algorithm: 'SHA-1' | 'SHA-256', data: ArrayBuffer): Promise<ArrayBuffer>;
    encrypt(algorithm: { name: 'RSA-OAEP' | 'AES-GCM'; iv?: ArrayBuffer }, spkiPublicKey: ICryptoKey, data: string): Promise<ArrayBuffer>;
    decrypt(algorithm: { name: 'RSA-OAEP' | 'AES-GCM'; iv?: ArrayBuffer }, pkcs8PrivateKey: ICryptoKey, data: ArrayBuffer): Promise<ArrayBuffer>;
}
