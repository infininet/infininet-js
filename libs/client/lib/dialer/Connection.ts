import { EventEmitter } from 'events';
import IWriter from './../stream/IWriter';
import IReader from './../stream/IReader';
import Writer from './../stream/Writer';
import Reader from './../stream/Reader';
import ISocket from './../stream/ISocket';
import IConnection from './../dialer/IConnection';

export default class Connection extends EventEmitter implements IConnection {
    private socket: ISocket;
    private writer: IWriter;
    private reader: IReader;

    public constructor(socket: ISocket) {
        super();
        this.socket = socket;

        this.socket.onDisconnect((): void => {
            this.emit('end');
        });

        this.writer = new Writer({ socket: socket });
        this.reader = new Reader({ socket: socket });
    }

    public onMessage(callback: (data: string) => void): void {
        this.reader.onData(data => callback(data));
    }

    public async close(): Promise<void> {
        return this.socket.close();
    }

    public async send(data: string): Promise<void> {
        return this.writer.write(data);
    }
}
