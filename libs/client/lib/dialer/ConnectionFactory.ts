import Connection from './Connection';
import IConnectionFactory from './IConnectionFactory';
import { IConnectOptions } from './../IConnector';
import ISocket from './../stream/ISocket';

export default class ConnectionFactory implements IConnectionFactory {
    public createNetworkConnection(socket: ISocket, options: IConnectOptions, instance: string): Promise<Connection> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.sendVersion(socket, options);
                await this.sendAppIdentifier(socket, options);
                await this.sendEnvironmentIdentifier(socket, options);
                await this.sendNetworkIdentifier(socket, options);
                await this.sendInstanceIdentifier(socket, instance);

                resolve(new Connection(socket));
            } catch (err) {
                reject(err);
            }
        });
    }

    protected async sendVersion(socket: ISocket, options: IConnectOptions): Promise<void> {
        await this.send(socket, options.version);
        if (await this.getNextMessage(socket) !== 'ACK') {
            throw new Error('Version failed');
        }
    }

    protected async sendAppIdentifier(socket: ISocket, options: IConnectOptions): Promise<void> {
        await this.send(socket, options.app);
        if (await this.getNextMessage(socket) !== 'ACK') {
            throw new Error('App failed');
        }
    }

    protected async sendEnvironmentIdentifier(socket: ISocket, options: IConnectOptions): Promise<void> {
        await this.send(socket, options.environment);
        if (await this.getNextMessage(socket) !== 'ACK') {
            throw new Error('Environment failed');
        }
    }

    protected async sendNetworkIdentifier(socket: ISocket, options: IConnectOptions): Promise<void> {
        await this.send(socket, options.network);
        if (await this.getNextMessage(socket) !== 'ACK') {
            throw new Error('Network failed');
        }
    }

    protected async sendInstanceIdentifier(socket: ISocket, instance: string): Promise<void> {
        await this.send(socket, instance);
        if (await this.getNextMessage(socket) !== 'HELLO;') {
            throw new Error('Instance failed');
        }
    }

    protected async getNextMessage(socket: ISocket): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                socket.onceData(async (data: string) => {
                    console.log('INCOMING', data.toString().trim());
                    resolve(data.toString().trim());
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    protected async send(socket: ISocket, data: string): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                console.log('SENDING', data.length, data.substr(0, 120), '...');
                await socket.write(data + '\n');
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }
}
