import { EventEmitter } from 'events';

export default interface IConnection extends EventEmitter {
    send(data: string): Promise<void>;
    onMessage(callback: (data: string) => void): void;
    close(): Promise<void>;
}
