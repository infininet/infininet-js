import { IRequestHandler } from './../ICommunicator';

export interface IHttpHandler {
    getHandler(): IRequestHandler;
    setUpProxyEndpoint(host: string, port: number): void;
}
