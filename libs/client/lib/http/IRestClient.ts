import { IResponse } from './../communicator/remote/http/IResponse';

export interface IRestClient {
    get(route: string, params: Record<string, unknown>): Promise<IResponse>
    post(route: string, params: Record<string, unknown>, body: string): Promise<IResponse>
    patch(route: string, params: Record<string, unknown>, body: string): Promise<IResponse>
    delete(route: string, params: Record<string, unknown>): Promise<IResponse>
}