import { IResponse } from '../communicator/remote/http/IResponse';
import { IRestClient } from './IRestClient';
import IConnector from './../IConnector';
import IRemote from './../communicator/IRemote';


export class RestClient implements IRestClient {
    private connector: IConnector;
    private endpoint: string;

    public constructor(connector: IConnector, endpoint: string) {
        this.connector = connector;
        this.endpoint = endpoint;
    }

    public async request(method: string, route: any, params: Record<string, unknown>, body?: string): Promise<IResponse> {
        let remote: IRemote;
        try {
            remote = await this.connector.connectEndpoint(this.endpoint);
        } catch (err) {
            console.log(err);
            return {
                status: 404,
                headers: {},
                body: err.message,
            }
        }

        try {
            return await remote.http(method, route, params, body);
        } catch (err) {
            console.log(err);
            return {
                status: 500,
                headers: {},
                body: err.message,
            }
        }
    }

    public async get(route: any, params: Record<string, unknown>): Promise<IResponse> {
        return this.request('GET', route, params);
    }

    public async post(route: any, params: Record<string, unknown>, body: string): Promise<IResponse> {
        return this.request('POST', route, params, body);
    }

    public async patch(route: any, params: Record<string, unknown>, body: string): Promise<IResponse> {
        return this.request('PATCH', route, params, body);
    }

    public async delete(route: any, params: Record<string, unknown>): Promise<IResponse> {
        return this.request('DELETE', route, params);
    }
}
