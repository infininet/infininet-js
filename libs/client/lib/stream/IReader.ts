import { EventEmitter } from 'events';

export default interface IReader extends EventEmitter{
    onData(callback: (data: string) => void): void;
}
