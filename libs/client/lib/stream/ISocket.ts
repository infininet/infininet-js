export default interface ISocket {
    close(): Promise<void>;
    write(message: string): Promise<void>;
    onData(callback: (data: string) => void): void;
    onceData(callback: (data: string) => void): void;
    onDisconnect(callback: () => void): void;
}
