export default interface IWriter {
    write(data: string): Promise<void>;
}
