import IReader from './IReader';
import ISocket from './ISocket';
import { EventEmitter } from 'events';

export interface IReaderOptions {
    socket: ISocket;
    maxBytes?: number;
    messageTeminator?: string;
}

export default class Reader extends EventEmitter implements IReader {
    private messageTerminator: string;
    private maxBytes: number;
    private data: string = '';

    public constructor(options: IReaderOptions) {
        super();

        this.maxBytes = options.maxBytes || 1024 * 1000 * 50; // ~50 MB
        this.messageTerminator = options.messageTeminator || ';';

        options.socket.onData((string: string) => {
            this.chunk(string);
        });
    }

    public onData(callback: (data: string) => void): void {
        this.on('message', data => {
            callback(data);
        });
    }

    public chunk(chunk: string): void {
        this.data += chunk.trim();
        if (this.data.length > this.maxBytes) {
            throw new Error(`Package Size of ${this.data.length} exceeded the allowed ${this.maxBytes}`);
        }

        if (chunk.indexOf(this.messageTerminator) !== -1) {
            let parts = this.data.split(this.messageTerminator);
            this.data = '';

            parts.forEach((part) => {
                if (part && part.trim().length > 0) {
                    this.emit('message', part);
                }
            });

            return;
        }
    }
}
