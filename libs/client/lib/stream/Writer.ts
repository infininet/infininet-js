import ISocket from './ISocket';
import IWriter from './IWriter';
import { EventEmitter } from 'events';

export interface IWriterOptions {
    socket: ISocket;
    messageTeminator?: string;
}

export default class Writer extends EventEmitter implements IWriter {
    private messageTerminator: string;
    private socket: ISocket;

    public constructor(options: IWriterOptions) {
        super();

        this.socket = options.socket;
        this.messageTerminator = options.messageTeminator || ';';
    }

    public async write(data: string): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                console.log('SENDING', data.length, data.substr(0, 120), '...');
                await this.socket.write(data + this.messageTerminator + '\n');
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }
}
