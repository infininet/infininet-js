export default interface IErrorHandler {
    handleError(requestId: string, errorCode: string, msg: string): Promise<void>;
    handleFatal(requestId: string, errorCode: string, msg: string): Promise<void>;
}
