import MessageKind from './MessageKind';

export interface IIncomingData {
    version: string;
    kind: MessageKind;
    requestId: string;
    from: string;
    to: string;
    payload: string;
    original: string;
}

export default interface IIncomingRequest {
    getKind(): MessageKind;
    getData(): IIncomingData;
}
