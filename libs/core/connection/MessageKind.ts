enum MessageKind {
    MSG = 'MSG',
    INF = 'INF',
    RES = 'RES',
}

export default MessageKind;
