export default interface IEncoder {
    md5(source: string): string;
    toBase64(utf8: string): string;
    fromBase64(base64: string): string;
    toBuffer(source: string): ArrayBuffer;
    serialize<T>(obj: T): string;
    unserialize<T>(serialized: string): T;
}