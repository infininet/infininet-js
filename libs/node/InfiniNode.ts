import IInfiniNodeContainer from './lib/IInfiniNodeContainer';

export default class InfiniNode {
    private serviceContainer: IInfiniNodeContainer;

    public constructor(serviceContainer: IInfiniNodeContainer) {
        this.serviceContainer = serviceContainer;
    }

    public async run(): Promise<void> {
        let Dispatcher = this.serviceContainer.getDispatcher();
        let server = this.serviceContainer.getServer();
        Dispatcher.connectServer(server);

        for (let connector of Array.from(this.serviceContainer.getConnectorSet())) {
            server.addConnector(connector);
            if (!await connector.start()) {
                throw new Error('Connector start failed');
            }
        }
    }
}
