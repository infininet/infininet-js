import IErrorHandler from './../core/connection/IErrorHandler';
import { IIncomingData } from './../core/connection/IIncomingRequest';

export default interface IConnection extends IErrorHandler{
    close(msg?: string): void;
    getApp(): string;
    getInstance(): string;
    getEnvironmentAddress(): string;
    getNetwork(): string;
    send(data: string): Promise<void>;
    onMessage(callback: (data: IIncomingData) => void): void;
    onDisconnect(callback: () => void): void;
}
