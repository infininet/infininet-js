import IConnection from './IConnection';

export default interface IConnector {
    start(): Promise<boolean>;
    onConnection(callback: (connection: IConnection) => void): void;
    onShutdown(callback: (err?: Error) => void): void;
}
