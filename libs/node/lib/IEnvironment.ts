import { INetwork } from './environment/INetwork';

export interface IEnvironmentSetup {
    address: string,
    networks: string[],
}

export interface IEnvironment {
    getAddress(): string;
    addNetwork(network: INetwork): void;
    getNetwork(address: string): INetwork | undefined;
}