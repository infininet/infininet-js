import IDispatcher from './peertopeer/IDispatcher';
import IConnector from './IConnector';
import IServer from './IServer';

export default interface IInfiniNodeContainer {
    getServer(): IServer;
    getDispatcher(): IDispatcher;
    getConnectorSet(): Set<IConnector>;
}
