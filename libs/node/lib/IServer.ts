import IConnection from './IConnection';
import IConnector from './IConnector';

export default interface IServer {
    onConnection(callback: (connection: IConnection) => void): void;
    addConnector(connector: IConnector): void;
}
