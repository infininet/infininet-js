import IConnection from './IConnection';
import IConnector from './IConnector';
import IServer from './IServer';
import { EventEmitter } from 'events';

export default class Server extends EventEmitter implements IServer {
    protected connectors: Set<IConnector> = new Set<IConnector>();

    public constructor() {
        super();
    }

    public addConnector(connector: IConnector): void {
        this.connectors.add(connector);

        connector.onConnection((connection: IConnection) => {
            void this.handle(connection);
        });

        connector.onShutdown(() => {
            this.connectors.delete(connector);
        });
    }

    protected async handle(connection: IConnection): Promise<void> {
        this.emit('connection', connection);
    }

    public onConnection(callback: (connection: IConnection) => void): void {
        this.on('connection', (connection: IConnection) => callback(connection));
    }
}
