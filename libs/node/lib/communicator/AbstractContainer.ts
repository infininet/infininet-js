import ConnectionFactory from './../communicator/ConnectionFactory';
import ErrorHandler from './../communicator/ErrorHandler';
import IConnectionFactory from './../communicator/IConnectionFactory';
import IContainer from './../communicator/IContainer';
import IErrorHandler from './../../core/connection/IErrorHandler';
import IReader from './../stream/IReader';
import ISocket from './../stream/ISocket';
import IWriter from './../stream/IWriter';
import { IRepository } from '@src/ext/infininet/lib/environment/IRepository';

export default abstract class AbstractContainer implements IContainer {
    public getConnectionFactory(): IConnectionFactory {
        return new ConnectionFactory();
    }

    public getErrorHandler(socket: ISocket): IErrorHandler {
        return new ErrorHandler(socket);
    }

    public abstract createReader(socket: ISocket): IReader;
    public abstract createWriter(socket: ISocket): IWriter;
    public abstract getEnvironmentRepository(): IRepository;
}
