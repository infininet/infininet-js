import IConnection from './../IConnection';
import IReader from './../stream/IReader';
import ISocket from './../stream/ISocket';
import IWriter from './../stream/IWriter';
import { EventEmitter } from 'events';
import { ErrorCode } from './../communicator/ErrorHandler';
import MessageKind from './../../core/connection/MessageKind';
import { IIncomingData } from './../../core/connection/IIncomingRequest';
import IErrorHandler from './../../core/connection/IErrorHandler';

export interface IConnectionOptions {
    version: string;
    app: string;
    environment: string;
    network: string;
    instance: string;
    socket: ISocket;
    writer: IWriter;
    reader: IReader;
    errorHandler: IErrorHandler;
}

export default class Connection extends EventEmitter implements IConnection {
    private version: string;
    private app: string;
    private environment: string;
    private network: string;
    private instance: string;
    private socket: ISocket;
    private reader: IReader;
    private writer: IWriter;
    private errorHandler: IErrorHandler;

    public constructor(options: IConnectionOptions) {
        super();
        this.socket = options.socket;
        this.app = options.app;
        this.environment = options.environment;
        this.network = options.network;
        this.instance = options.instance;
        this.reader = options.reader;
        this.writer = options.writer;
        this.version = options.version;
        this.errorHandler = options.errorHandler;

        this.receive();
    }

    public handleError(requestId: string, errorCode: string, msg: string): Promise<void> {
        return this.errorHandler.handleError(requestId, errorCode, msg);
    }

    public handleFatal(requestId: string, errorCode: string, msg: string): Promise<void> {
        return this.errorHandler.handleFatal(requestId, errorCode, msg);
    }

    public getVersion(): string {
        return this.version;
    }

    public getApp(): string {
        return this.app;
    }

    public getEnvironmentAddress(): string {
        return this.environment;
    }

    public getNetwork(): string {
        return this.network;
    }

    public getInstance(): string {
        return this.instance;
    }

    public async send(data: string): Promise<void> {
        console.log('SENDING', data.length, data.substr(0, 120), '...');

        return this.writer.send(data);
    }

    public onMessage(callback: (data: IIncomingData) => void): void {
        this.on('message', data => callback(data));
    }

    public onDisconnect(callback: () => void): void {
        this.socket.onDisconnect(callback);
    }

    protected getValidMessageKind(kindCandidate: string): MessageKind {
        let kind: MessageKind = kindCandidate.toUpperCase() as MessageKind;
        if (!Object.values(MessageKind).includes(kind)) {
            throw new Error('Kind not supported');
        }

        return kind;
    }

    protected receive(): void {
        this.reader.onData((data: string) => {
            let kind;
            let requestId;
            try {
                let parts = data.split('.');

                kind = <string>parts.shift();
                requestId = <string>parts.shift();

                this.emit('message', <IIncomingData>{
                    version: this.version,
                    kind: this.getValidMessageKind(kind),
                    requestId: requestId,
                    from: <string>parts.shift(),
                    to: <string>parts.shift(),
                    payload: <string>parts.shift(),
                    original: data,
                });
            } catch (err) {
                console.log(err);
                void this.handleFatal(requestId ?? 'unknown', ErrorCode.INTERNAL, 'Internal Error');
            }
        });
    }

    public async close(): Promise<void> {
        return this.socket.close();
    }
}
