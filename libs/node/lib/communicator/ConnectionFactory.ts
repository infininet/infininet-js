import Connection from './../communicator/Connection';
import IContainer from './../communicator/IContainer';
import { ErrorCode } from './../communicator/ErrorHandler';
import IConnectionFactory from './../communicator/IConnectionFactory';
import IConnection from './../IConnection';
import ISocket from './../stream/ISocket';

export default class ConnectionFactory implements IConnectionFactory {
    public createConnection(socket: ISocket, depContainer: IContainer): Promise<IConnection> {
        return new Promise(async (resolve, reject) => {
            try {
                let version = await this.getNextMessage(socket);
                this.checkVersion(version);
                await this.send(socket, 'ACK');

                resolve(await this.createConnectionByVersion(version, socket, depContainer));
            } catch (err) {
                void depContainer.getErrorHandler(socket).handleFatal('init', ErrorCode.INTERNAL, err.message);

                reject(err);
            }
        });
    }

    protected async createConnectionByVersion(version: string, socket: ISocket, depContainer: IContainer): Promise<IConnection> {
        return new Promise(async (resolve, reject) => {
            try {
                // Request App
                let appIdentifier = await this.getNextMessage(socket);
                this.checkAppIdentifierPlausibility(appIdentifier);
                await this.send(socket, 'ACK');

                // Request Environment
                let environmentIdentifier = await this.getNextMessage(socket);
                this.checkEnvironmentIdentifierPlausibility(environmentIdentifier);
                await this.send(socket, 'ACK');

                // Request Network
                let networkIdentifier = await this.getNextMessage(socket);
                this.checkNetworkIdentifierPlausibility(networkIdentifier);
                this.checkNetworkExists(environmentIdentifier, networkIdentifier, depContainer);

                await this.send(socket, 'ACK');

                // Request Peer Instance
                let peerInstance = await this.getNextMessage(socket);
                this.checkPeerIdentifierPlausibility(peerInstance);

                resolve(new Connection({
                    version: version,
                    app: appIdentifier,
                    environment: environmentIdentifier,
                    network: networkIdentifier,
                    instance: peerInstance,
                    socket: socket,
                    reader: depContainer.createReader(socket),
                    writer: depContainer.createWriter(socket),
                    errorHandler: depContainer.getErrorHandler(socket),
                }));
            } catch (err) {
                reject(err);
            }
        });
    }

    protected checkNetworkExists(environmentAddress: string, networkAddress: string, depContainer: IContainer) {
        return depContainer.getEnvironmentRepository().getNetwork(environmentAddress, networkAddress) !== undefined;
    }

    protected async getNextMessage(socket: ISocket): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                socket.onceData((data: string) => {
                    resolve(data.toString().trim());
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    protected async send(socket: ISocket, data: string): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                await socket.write(data + '\n');
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }

    protected checkVersion(version: string): void {
        if (!version.match(/^[0-9]{1,3}\.[0-9]{1,5}\.[0-9]{1,10}$/gi)) {
            throw new Error('Unqualified Version');
        }
    }

    protected checkAppIdentifierPlausibility(appIdentifier: string): void {
        if (!appIdentifier.match(/^[a-zA-Z0-9-_]{2,36}$/gi)) {
            throw new Error('Unqualified App');
        }
    }

    protected checkEnvironmentIdentifierPlausibility(networkIdentifier: string): void {
        if (!networkIdentifier.match(/^[a-zA-Z0-9-_]{3,36}$/gi)) {
            throw new Error('Unqualified Environment');
        }
    }

    protected checkNetworkIdentifierPlausibility(networkIdentifier: string): void {
        if (!networkIdentifier.match(/^[a-zA-Z0-9-_]{3,36}$/gi)) {
            throw new Error('Unqualified Network');
        }
    }

    protected checkPeerIdentifierPlausibility(instance: string): void {
        if (!instance.match(/^[a-zA-Z0-9-_]{3,64}$/gi)) {
            throw new Error('Unqualified Instance');
        }
    }
}
