import IErrorHandler from './../../core/connection/IErrorHandler';
import ISocket from './../stream/ISocket';

export enum ErrorCode {
    INTERNAL = '0000',
}

export default class ErrorHandler implements IErrorHandler {
    public socket: ISocket;

    public constructor(socket: ISocket) {
        this.socket = socket;
    }

    public async handleError(requestId: string, errorCode: string, msg: string): Promise<void> {
        let errorResponse = `ERR.${requestId}.${errorCode}.${Buffer.from(msg).toString('base64')};\n`;
        console.log('errorResponse', errorResponse);
        await this.socket.write(errorResponse);
    }

    public async handleFatal(requestId: string, errorCode: string, msg: string): Promise<void> {
        await this.handleError(requestId, errorCode, msg);
        void this.socket.close();
    }
}
