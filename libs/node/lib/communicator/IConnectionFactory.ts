import IContainer from './../communicator/IContainer';
import IConnection from './../IConnection';
import ISocket from './../stream/ISocket';

export default interface IConnectionFactory {
    createConnection(socket: ISocket, depContainer: IContainer): Promise<IConnection>;
}
