import IReader from './../stream/IReader';
import IErrorHandler from './../../core/connection/IErrorHandler';
import ISocket from './../stream/ISocket';
import IWriter from './../stream/IWriter';
import IConnectionFactory from './../communicator/IConnectionFactory';
import { IRepository } from './../environment/IRepository';

export default interface IContainer {
    createReader(socket: ISocket): IReader;
    createWriter(socket: ISocket): IWriter;
    getConnectionFactory(): IConnectionFactory;
    getErrorHandler(socket: ISocket): IErrorHandler;
    getEnvironmentRepository(): IRepository
}
