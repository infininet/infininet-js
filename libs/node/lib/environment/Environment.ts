import { IEnvironment } from '../IEnvironment';
import { INetwork } from './INetwork';

export class Environment implements IEnvironment {
    public address: string;
    public networks: Map<string, INetwork> = new Map<string, INetwork>();

    public constructor(address: string) {
        this.address = address;
    }

    public getAddress(): string {
        return this.address;
    }

    public addNetwork(network: INetwork): void {
        if (this.networks.has(network.getAddress())) {
            throw new Error(`Network ${network.getAddress()} is already defined.`);
        }

        this.networks.set(network.getAddress(), network);
    }

    public getNetwork(address: string): INetwork | undefined {
        return this.networks.get(address);
    }
}
