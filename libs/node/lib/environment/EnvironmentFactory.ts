import { Environment } from './Environment';
import { Repository } from './Repository';
import { IEnvironment, IEnvironmentSetup } from '../IEnvironment';
import { IRepository } from './IRepository';
import { INetworkFactory } from '@src/ext/infininet/lib/environment/INetworkFactory';

export class EnvironmentFactory {
    public generateRepository(environmentSetups: IEnvironmentSetup[], networkFactory: INetworkFactory): IRepository {
        let environments: IEnvironment[] = [];
        for (let setup of environmentSetups) {
            let environment = new Environment(setup.address);
            console.log(`Environment ${environment.getAddress()} created`)
            for (let networkAddress of setup.networks) {
                environment.addNetwork(networkFactory.createNetwork(setup.address, networkAddress));
                console.log(`Network ${networkAddress} created`)
            }

            environments.push(environment);
        }

        return new Repository(environments);
    }
}