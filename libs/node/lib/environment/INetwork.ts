export interface INetwork {
    getEnvironmentAddress(): string;
    getAddress(): string;
}