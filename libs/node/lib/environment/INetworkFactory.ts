import { INetwork } from '@src/ext/infininet/lib/environment/INetwork';

export interface INetworkFactory {
    createNetwork(environmentAddress: string, networkAddress: string): INetwork;
}
