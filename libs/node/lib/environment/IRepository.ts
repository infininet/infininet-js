import { IEnvironment } from '../IEnvironment';
import { INetwork } from './INetwork';

export interface IRepository {
    getEnvironment(address: string): IEnvironment | undefined;
    getNetwork(environmentAddress: string, networkAddress: string): INetwork | undefined;
}
