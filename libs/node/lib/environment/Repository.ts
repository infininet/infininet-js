import { IEnvironment } from '../IEnvironment';
import { INetwork } from './INetwork';
import { IRepository } from './IRepository';

export class Repository implements IRepository {
    private environments: Map<string, IEnvironment> = new Map<string, IEnvironment>();

    public constructor(environments: IEnvironment[]) {
        for (let environment of environments) {
            console.log('add', environment)
            this.setEnvironment(environment);
        }
    }

    protected setEnvironment(environment: IEnvironment): void {
        if (this.environments.has(environment.getAddress())) {
            throw new Error(`Environment '${environment.getAddress()}' has been already defined`);
        }

        this.environments.set(environment.getAddress(), environment);
    }

    public getEnvironment(address: string): IEnvironment | undefined {
        return this.environments.get(address);
    }

    public getNetwork(environmentKey: string, networkKey: string): INetwork | undefined {
        console.log('env', this.getEnvironment(environmentKey), environmentKey);
        console.log('net', this.getEnvironment(environmentKey)?.getNetwork(networkKey), networkKey)
        return this.getEnvironment(environmentKey)?.getNetwork(networkKey);
    }
}
