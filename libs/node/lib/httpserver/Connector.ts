import IHttpContainer from './../httpserver/IHttpContainer';
import IWebSocketConnection from './../httpserver/IWebSocketConnection';
import Socket from './../httpserver/Socket';
import ISocketServer from './../netserver/ISocketServer';
import IConnection from './../IConnection';
import IConnector from './../IConnector';
import { EventEmitter } from 'events';
import http, { IncomingMessage, Server, ServerResponse } from 'http';
import ws from 'ws';
import Reader from '@src/ext/infininet/lib/httpserver/request/Reader';
import { UnauthorizedError, UnknownRouteError } from '@src/ext/infininet/lib/httpserver/request/IRequestRouter';


export default class Connector extends EventEmitter implements IConnector {
    protected readonly EVENT_LISTENING: string = 'listening';
    protected readonly EVENT_ERROR: string = 'error';
    protected readonly EVENT_END: string = 'end';
    protected readonly EVENT_CLOSE: string = 'close';
    protected readonly EVENT_CONNECTION: string = 'connection';

    private depContainer: IHttpContainer;
    private server?: ISocketServer;
    private _oWsServer?: ws.Server;
    private online: boolean;

    public constructor(depContainer: IHttpContainer) {
        super();
        this.depContainer = depContainer;
    }

    public async start(): Promise<boolean> {
        return this.createServer();
    }

    private async handleRequest(req: IncomingMessage, res: ServerResponse): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                let result = await this.depContainer.getHttpRouter()
                    .dispatch(await (new Reader(req)).evaluate({
                        max_body_size: 1024,
                    }));

                res.writeHead(200);
                res.end(JSON.stringify(result || {}));
            } catch (err) {

                console.log(err);

                if (err instanceof UnauthorizedError) {
                    res.writeHead(401);
                    res.end(JSON.stringify({ error: err.message }));
                } else if (err instanceof UnknownRouteError) {
                    res.writeHead(404);
                    res.end(JSON.stringify({ error: err.message }));
                } else {
                    res.writeHead(500);
                    res.end(JSON.stringify({ error: 'Internal Error' }));
                }
            }
        });
    }

    public async createServer(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                this.server = http.createServer(async (req: IncomingMessage, res: ServerResponse) => {
                    try {
                        await this.handleRequest(req, res);
                    } catch (err) {
                        console.log(err);
                        res.end(JSON.stringify({ error: 'Internal Error' }));
                    }
                });

                this.server.once(this.EVENT_LISTENING, () => {
                    this.online = true;
                    console.log(`Http Server listening on port ${this.depContainer.getPort()}`);
                    resolve(true);
                });

                this.server.once(this.EVENT_CLOSE, () => {
                    void this.handleShutdown();
                });
                this.server.once(this.EVENT_END, () => {
                    void this.handleShutdown();
                });
                this.server.on(this.EVENT_ERROR, (err: Error) => {
                    console.log(err);
                });


                // Creating Websocket Server
                this._oWsServer = new ws.Server({ server: <Server> this.server });
                this._oWsServer.on('connection', async  (socket: IWebSocketConnection) => {
                    console.log('GOT WEBSOCKET CONNECTION');
                    void this.handleConnectionRequest(socket);
                });

                this._oWsServer.on('listening', () => {
                    console.log(`WebSocket Server listening ${this.depContainer.getPort()}`);
                });

                this._oWsServer.on('close', () => {
                    console.log('WebSocket Server down');
                });

                this.server.listen(this.depContainer.getPort());
            } catch (err) {
                reject(err);
            }
        });
    }

    public shutdown(): void {
        if (this.online && this.server) {
            this.server.close();
            delete this.server;
        }
    }

    protected async handleConnectionRequest(socket: IWebSocketConnection): Promise<void> {
        try {
            this.emit(
                'connection',
                await this.depContainer.getConnectionFactory().createConnection(
                    new Socket(socket),
                    this.depContainer,
                ),
            );
        } catch (err) {
            console.log(err);
        }
    }

    protected async handleShutdown(): Promise<void> {
        this.shutdown();
        this.online = false;
        this.emit('end');
    }

    public onConnection(callback: (connection: IConnection) => void): void {
        this.on('connection', async (connection: IConnection) => {
            callback(connection);
        });
    }

    public onShutdown(callback: (err?: Error) => void): void {
        this.on('end', () => callback);
    }
}
