import IContainer from './../communicator/IContainer';
import IConnectionFactory from './../communicator/IConnectionFactory';
import IErrorHandler from './../../core/connection/IErrorHandler';
import ISocket from './../stream/ISocket';
import { IRequestRouter } from '@src/ext/infininet/lib/httpserver/request/IRequestRouter';

export default interface IHttpContainer extends IContainer{
    getPort(): number;
    getConnectionFactory(): IConnectionFactory;
    getErrorHandler(socket: ISocket): IErrorHandler;
    getHttpRouter(): IRequestRouter;
}
