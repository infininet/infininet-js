export type IMessageListenerCallback = (message: string) => void;
export type ICloseCallback = () => void;
export type IMessageSenderCallback = (err: Error | undefined) => void;
export type IErrorCallback = (err: Error) => void;

export default interface IWebSocketConnection {
    on(event: 'message', callback: IMessageListenerCallback): void;
    on(event: 'error', callback: IErrorCallback): void;
    on(event: 'close', callback: ICloseCallback): void;
    once(event: 'message', callback: IMessageListenerCallback): void;
    send(data: string, callback?: IMessageSenderCallback): void;
    close(): void;
}
