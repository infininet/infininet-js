import IWebSocketConnection from './../httpserver/IWebSocketConnection';
import ISocket from './../stream/ISocket';

export default class Socket implements ISocket {
    protected webSocket: IWebSocketConnection;

    public constructor(netSocket: IWebSocketConnection) {
        this.webSocket = netSocket;

        this.webSocket.on('error', (err: Error) => {
            console.log(err);
        });
    }

    public async close(): Promise<void> {
        this.webSocket.close();
    }

    public async write(message: string): Promise<void> {
        return new Promise((resolve, reject) => {
            try {
                this.webSocket.send(message, (err?: Error) => {
                    if (err) {
                        reject(err);
                    }

                    resolve();
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    public onData(callback: (data: string) => void): void {
        this.webSocket.on('message', data => callback(data.toString()));
    }

    public onceData(callback: (data: string) => void): void {
        this.webSocket.once('message', data => callback(data.toString()));
    }

    public onDisconnect(callback: () => void): void {
        this.webSocket.on('close', callback);
    }
}
