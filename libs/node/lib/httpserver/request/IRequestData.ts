export interface IRequestData {
    method: string;
    headers: Record<string, unknown>;
    route: string,
    params: Record<string, unknown>;
    payload: string;
}