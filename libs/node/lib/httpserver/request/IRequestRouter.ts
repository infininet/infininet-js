import { IRequestData } from './IRequestData';
import { IRoute } from './IRoute';

export class UnknownRouteError extends Error {};
export class UnauthorizedError extends Error {};
export class InternalError extends Error {};

export interface IRequestRouter {
    defineRoute(route: IRoute): void;
    dispatch(requestData: IRequestData): Promise<Record<string, unknown>>;
}