export interface IRoute {
    match(routeKey: string): boolean;
    dispatch(args: { params: Record<string, unknown>; payload?: string }): Promise<Record<string, unknown>>;
}
