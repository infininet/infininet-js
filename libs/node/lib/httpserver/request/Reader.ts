import { IncomingMessage } from 'http';
import { IRequestData } from './IRequestData';

import querystring from 'querystring';

export default class Reader {
    private httpReq: IncomingMessage;

    constructor(httpReq: IncomingMessage) {
        this.httpReq = httpReq;
    }

    public async evaluate(options: { max_body_size: number }): Promise<IRequestData> {
        return new Promise(async (resolve, reject) => {
            try {
                resolve({
                    method: this.evaluateMethod(),
                    headers: this.httpReq.headers,
                    params: this.evaluateParams(),
                    route: this.evaluateRoute(),
                    payload: await this.evaluatePayload(options.max_body_size),
                });
            } catch (err) {
                reject(err);
            }
        });
    }

    protected evaluateMethod(): string {
        return this.httpReq.method?.toUpperCase() ?? '';
    }

    protected evaluateParams(): Record<string, unknown> {
        if (this.httpReq.url) {
            let parts = this.httpReq.url.split('?');
            if (parts[1] !== undefined) {
                return querystring.parse(parts[1]);
            }
        }

        return {};
    }

    protected evaluateRoute(): string {
        if (this.httpReq.url) {
            let parts = this.httpReq.url.split('?');
            if (parts[0] !== undefined) {
                return parts[0];
            }
        }

        return '/';
    }

    protected async evaluatePayload(max_body_size: number): Promise<string> {
        if (!['POST', 'PUT'].includes(this.evaluateMethod())) {
            return '';
        }

        return new Promise((resolve, reject) => {
            try {
                let data = '';
                this.httpReq.on('data', (chunk: Buffer) => {
                    data += chunk.toString();
                    console.log(`Received chunk of ${chunk.toString().length} Byte`);
                    if (data.length > max_body_size) {
                        this.httpReq.destroy();
                        reject(new Error("Request too large"));
                    }
                });
                this.httpReq.once('end', () => {
                    resolve(data);
                })
                this.httpReq.on('error', (err) => {
                    reject(err);
                })
            } catch (err) {
                reject(err);
            }
        });
    }
}