import INetServerContainer from './../netserver/INetServerContainer';
import ISocketServer from './../netserver/ISocketServer';
import Socket from './../netserver/Socket';
import IConnection from './../IConnection';
import IConnector from './../IConnector';
import { EventEmitter } from 'events';
import net from 'net';

export default class Connector extends EventEmitter implements IConnector {
    protected readonly EVENT_LISTENING: string = 'listening';
    protected readonly EVENT_ERROR: string = 'error';
    protected readonly EVENT_END: string = 'end';
    protected readonly EVENT_CLOSE: string = 'close';
    protected readonly EVENT_CONNECTION: string = 'connection';

    private depContainer: INetServerContainer;
    private server?: ISocketServer;
    private online: boolean;

    public constructor(netService: INetServerContainer) {
        super();
        this.depContainer = netService;
    }

    public async start(): Promise<boolean> {
        return this.createServer();
    }

    public async createServer(): Promise<boolean> {
        return new Promise((resolve, reject) => {
            try {
                this.server = net.createServer();
                this.server.once(this.EVENT_LISTENING, () => {
                    this.online = true;
                    console.log(`UnixSocket Server listen on port ${this.depContainer.getPort()}`);
                    resolve(true);
                });

                this.server.once(this.EVENT_CLOSE, () => {
                    void this.handleShutdown();
                });
                this.server.once(this.EVENT_END, () => {
                    void this.handleShutdown();
                });
                this.server.on(this.EVENT_CONNECTION, (socket: net.Socket) => {
                    void this.handleConnectionRequest(socket);
                });
                this.server.on(this.EVENT_ERROR, (err: Error) => {
                    console.log(err);
                });

                this.server.listen(this.depContainer.getPort());
            } catch (err) {
                reject(err);
            }
        });
    }

    public shutdown(): void {
        if (this.online && this.server) {
            this.server.close();
            delete this.server;
        }
    }

    protected async handleConnectionRequest(socket:  net.Socket): Promise<void> {
        try {
            this.emit(
                'connection',
                await this.depContainer.getConnectionFactory().createConnection(
                    new Socket(socket),
                    this.depContainer,
                ),
            );
        } catch (err) {
            console.log(err);
        }
    }

    protected async handleShutdown(): Promise<void> {
        this.shutdown();
        this.online = false;
        this.emit('end');
    }

    public onConnection(callback: (connection: IConnection) => void): void {
        this.on('connection', async (connection: IConnection) => {
            callback(connection);
        });
    }

    public onShutdown(callback: (err?: Error) => void): void {
        this.on('end', () => callback);
    }
}
