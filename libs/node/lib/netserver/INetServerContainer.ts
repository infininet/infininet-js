import IErrorHandler from './../../core/connection/IErrorHandler';
import ISocket from './../stream/ISocket';
import IContainer from './../communicator/IContainer';

export default interface INetServerContainer extends IContainer {
    getPort(): number;
    getErrorHandler(socket: ISocket): IErrorHandler;
}
