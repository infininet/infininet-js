import { EventEmitter } from 'events';

export default interface ISocketServer extends EventEmitter {
    listen(port: number): void;
    close(): void;
}
