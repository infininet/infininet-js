import IDispatcher from './IDispatcher';
import IDispatcherContainer from './IDispatcherContainer';
import Network from './Network';
import Peer from './Peer';
import IConnection from './../IConnection';
import IServer from './../IServer';
import { IRepository } from './../environment/IRepository';

export default class Dispatcher implements IDispatcher {
    private server?: IServer;
    private dependencyContainer: IDispatcherContainer;
    private environmentRepository: IRepository;

    public constructor(dependencyContainer: IDispatcherContainer, environmentRepository: IRepository) {
        this.dependencyContainer = dependencyContainer;
        this.environmentRepository = environmentRepository;
    }

    public connectServer(server: IServer): void {
        this.server = server;

        this.server.onConnection((connection: IConnection) => {
            this.handle(connection);
        });
    }

    protected handle(connection: IConnection): void {
        try {
            this.connectPeer(new Peer(connection, this.dependencyContainer.getMessageFactory()));
        } catch (err) {
            console.log(err);
            connection.close();
        }
    }

    protected connectPeer(peer: Peer): void {
        let network = this.environmentRepository.getNetwork(peer.getEnvironment(), peer.getNetwork());
        if (!network) {
            throw new Error(`Unknown Network '${peer.getNetwork()}'`)
        }

        (<Network>network).connectPeer(peer);
    }
}
