import IServer from './../IServer';

export default interface IDispatcher {
    connectServer(server: IServer): void;
}
