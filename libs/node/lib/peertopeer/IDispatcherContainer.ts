import IMessageFactory from './message/IMessageFactory';

export default interface IDispatcherContainer {
    getMessageFactory(): IMessageFactory;
}
