import { INetwork } from './../environment/INetwork';
import IResponseDataFactory from './message/IResponseDataFactory';
import Peer from './Peer';
import ErrorCode from './peer/ErrorCode';
import IInfoRequestData from './peer/IInfoRequestData';
import IMessageData from './peer/IMessageData';

export default class Network implements INetwork {
    private environmentAddress: string;
    private address: string;
    private peers: Map<string, Peer> = new Map<string, Peer>();
    private apps: Map<string, Set<Peer>> = new Map<string, Set<Peer>>();

    private responseDataFactory: IResponseDataFactory;

    public constructor(environmentAddress: string, address: string, responseDataFactory: IResponseDataFactory) {
        this.environmentAddress = environmentAddress;
        this.address = address;
        this.responseDataFactory = responseDataFactory;
    }

    public getEnvironmentAddress(): string {
        return this.environmentAddress;
    }

    public getAddress(): string {
        return this.address;
    }

    public connectPeer(peer: Peer): void {
        try {
            if (this.peers.has(peer.getInstance())) {
                console.log('[PEER][ALREADY-CONNECTED]', peer.getInstance());
                throw new Error(`Peer ${peer.getInstance()} already connected`);
            }

            peer.onDisconnect(() => {
                this.removePeer(peer);
            });
            peer.onMessage(async (message: IMessageData) => {
                try {
                    await this.dispatchMessage(peer, message);
                } catch (err) {
                    console.log(err);
                    void peer.handleError(message.requestId, ErrorCode.TRANSMISSION_FAILED, err.message);
                }
            });

            peer.onInfoRequest(async (data: IInfoRequestData) => {
                try {
                    await this.dispatchInfoRequest(peer, data);
                } catch (err) {
                    console.log(err);
                    void peer.handleError(data.requestId, ErrorCode.INTERNAL, err.message);
                }
            });

            void peer.init();

            this.peers.set(peer.getInstance(), peer);
            this.registerApp(peer);

            void peer.sayHello();

            console.log('[PEER][CONNECTED]', peer.getInstance());
        } catch (err) {
            console.log(err);
            void peer.handleFatal('connect', ErrorCode.INTERNAL, err.message);
        }
    }

    protected getApp(appId: string): Peer | undefined {
        return this.apps.get(appId)?.values().next().value;
    }

    protected async dispatchMessage(sender: Peer, messageData: IMessageData): Promise<void> {
        let recipient = this.getApp(messageData.to) ?? this.peers.get(messageData.to);
        if (!recipient) {
            void sender.handleError(messageData.requestId, ErrorCode.RECIPIENT_UNKNOWN, 'Unknown Recipient');

            return;
        }

        if (recipient === sender) {
            return;
        }

        await recipient.sendMessage(this.responseDataFactory.formatMsgTransferable(messageData));
    }

    protected async dispatchInfoRequest(sender: Peer, messageData: IInfoRequestData): Promise<void> {
        let instance = this.peers.get(messageData.payload);
        if (!instance) {
            void sender.handleError(messageData.requestId, ErrorCode.RECIPIENT_UNKNOWN, 'Unknown Instance');

            return;
        }

        await sender.sendMessage(this.responseDataFactory.formatInfoResponse(messageData, instance));
    }

    protected removePeer(peer: Peer): void {
        this.peers.delete(peer.getInstance());
        this.removeApp(peer);

        console.log('[PEER][DISCONNECTED]', peer.getInstance());
    }

    protected registerApp(peer: Peer): void {
        let apps = this.apps.get(peer.getApp());
        if (!apps) {
            apps = new Set<Peer>();
            this.apps.set(peer.getApp(), apps);
        }

        apps.add(peer);

        console.log(`[APP][${peer.getApp()}][CONNECTED]`, peer.getInstance());
        console.log(`[APP][${peer.getApp()}][INSTANCES]`, apps.size);
    }

    protected removeApp(peer: Peer): void {
        let apps = this.apps.get(peer.getApp());
        if (apps) {
            apps.delete(peer);
            console.log(`[APP][${peer.getApp()}][DISCONNECTED]`, peer.getInstance());

            if (apps.size === 0) {
                this.apps.delete(peer.getApp());
                console.log(`[APP][${peer.getApp()}][INSTANCES]`, 0);

                return;
            }

            console.log(`[APP][${peer.getApp()}][INSTANCES]`, apps.size);
        }
    }
}
