import { INetworkFactory } from './../environment/INetworkFactory';
import Network from './Network';
import IResponseDataFactory from './message/IResponseDataFactory';

export class NetworkFactory implements INetworkFactory {
    private responseDataFactory: IResponseDataFactory;

    public constructor(responseDataFactory: IResponseDataFactory) {
        this.responseDataFactory = responseDataFactory;
    }

    public createNetwork(environmentAddress: string, networkAddress: string) {
        return new Network(environmentAddress, networkAddress, this.responseDataFactory);
    }
}
