import IConnection from './../IConnection';
import MessageKind from './../../core/connection/MessageKind';
import IErrorHandler from './../../core/connection/IErrorHandler';
import { IIncomingData } from './../../core/connection/IIncomingRequest';
import IRequestDataFactory from './message/IRequestDataFactory';
import IMessageData from './peer/IMessageData';
import IInfoRequestData from './peer/IInfoRequestData';
import ITransferable from './peer/ITransferable';

export default class Peer implements IErrorHandler {
    private connection: IConnection;
    private messageFactory: IRequestDataFactory;
    private msgListener: Array<(message: IMessageData) => void> = [];
    private infListener: Array<(message: IInfoRequestData) => void> = [];

    public constructor(connection: IConnection, messageFactory: IRequestDataFactory) {
        this.connection = connection;
        this.messageFactory = messageFactory;
    }

    public handleError(requestId: string, errorCode: string, msg: string): Promise<void> {
        return this.connection.handleError(requestId, errorCode, msg);
    }

    public handleFatal(requestId: string, errorCode: string, msg: string): Promise<void> {
        return this.connection.handleFatal(requestId, errorCode, msg);
    }

    public getInstance(): string {
        return this.connection.getInstance();
    }

    public getApp(): string {
        return this.connection.getApp();
    }

    public getNetwork(): string {
        return this.connection.getNetwork();
    }

    public getEnvironment(): string {
        return this.connection.getEnvironmentAddress();
    }

    public async sayHello(): Promise<void> {
        return this.connection.send('HELLO');
    }

    public async disconnect(msg?: string): Promise<void> {
        this.connection.close(msg);
    }

    public async sendMessage(message: ITransferable): Promise<void> {
        await this.connection.send(message.getDataString());
    }

    public async init(): Promise<void> {
        this.connection.onMessage((data: IIncomingData) => {
            try {
                if (data.kind === MessageKind.MSG || data.kind === MessageKind.RES) {
                    for (let callback of this.msgListener) {
                        callback(this.messageFactory.formatMessageData(data));
                    }
                } else if (data.kind === MessageKind.INF) {
                    for (let callback of this.infListener) {
                        callback(this.messageFactory.formatInfoData(data));
                    }
                } else {
                    throw new Error(`Unsupported kind '${data.kind}'`);
                }
            } catch (err) {
                console.log(err);
            }
        });
    }

    public onMessage(callback: (message: IMessageData) => void): void {
        this.msgListener.push(callback);
    }

    public onInfoRequest(callback: (message: IInfoRequestData) => void): void {
        this.infListener.push(callback);
    }

    public onDisconnect(callback: () => void): void {
        this.connection.onDisconnect(() => callback());
    }
}
