import IRequestDataFactory from './../message/IRequestDataFactory';
import IResponseDataFactory from './../message/IResponseDataFactory';

export default interface IMessageFactory extends IRequestDataFactory, IResponseDataFactory {

}
