import IInfoRequestData from './../peer/IInfoRequestData';
import IMessageData from './../peer/IMessageData';
import { IIncomingData } from './../../../core/connection/IIncomingRequest';

export default interface IRequestDataFactory {
    formatMessageData(raw: IIncomingData): IMessageData;
    formatInfoData(raw: IIncomingData): IInfoRequestData;
}
