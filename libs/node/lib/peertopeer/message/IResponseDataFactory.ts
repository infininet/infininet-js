import Peer from './../Peer';
import IInfoRequestData from './../peer/IInfoRequestData';
import IMessageData from './../peer/IMessageData';
import ITransferable from './../peer/ITransferable';

export default interface IResponseDataFactory {
    formatInfoResponse(data: IInfoRequestData, peer: Peer): ITransferable;
    formatMsgTransferable(data: IMessageData): ITransferable;
}
