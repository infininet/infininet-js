import IMessageFactory from './../message/IMessageFactory';
import Peer from './../Peer';
import IInfoRequestData from './../peer/IInfoRequestData';
import IMessageData from './../peer/IMessageData';
import ITransferable from './../peer/ITransferable';
import { IIncomingData } from './../../../core/connection/IIncomingRequest';

export default class MessageFactory implements IMessageFactory {
    public formatInfoData(incommingData: IIncomingData): IInfoRequestData {
        // TODO - validate
        return incommingData;
    }

    public formatMessageData(incommingData: IIncomingData): IMessageData {
        // TODO - validate
        return incommingData;
    }

    public formatInfoResponse(data: IInfoRequestData, peer: Peer): ITransferable {
        return <ITransferable> {
            getDataString(): string {
                return [
                    'RES',
                    data.requestId,
                    data.from,
                    data.from,
                    Buffer.from(JSON.stringify({
                        network: peer.getNetwork(),
                        app: peer.getApp(),
                        instance: peer.getInstance(),
                    })).toString('base64'),
                ].join('.');
            },
        };
    }

    public formatMsgTransferable(data: IMessageData): ITransferable {
        return <ITransferable> {
            getDataString(): string {
                return data.original;
            },
        };
    }
}
