enum ErrorCode {
    INTERNAL = '1000',
    RECIPIENT_UNKNOWN = '1001',
    TRANSMISSION_FAILED = '1002',
    INSTANCE_UNKNOWN = '1003',
}

export default ErrorCode;
