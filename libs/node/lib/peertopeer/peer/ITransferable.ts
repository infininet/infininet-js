export default interface ITransferable {
    getDataString(): string;
}
