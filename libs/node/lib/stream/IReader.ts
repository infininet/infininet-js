export default interface IReader {
    onData(callback: (data: string) => void): void;
}
