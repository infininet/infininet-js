export default interface IWriter {
    send(data: string): Promise<void>;
}
