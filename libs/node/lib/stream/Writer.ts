import ISocket from './ISocket';
import IWriter from './IWriter';
import { EventEmitter } from 'events';

export interface IWriterOptions {
    socket: ISocket;
    messageTerminator?: string;
}

export default class Writer extends EventEmitter implements IWriter {
    private messageTerminator: string;
    private socket: ISocket;

    public constructor(options: IWriterOptions) {
        super();

        this.socket = options.socket;
        this.messageTerminator = options.messageTerminator || ';';
    }

    public async send(data: string): Promise<void> {
        return new Promise(async (resolve, reject) => {
            try {
                await this.socket.write(data + this.messageTerminator + '\n');
                resolve();
            } catch (err) {
                reject(err);
            }
        });
    }
}
